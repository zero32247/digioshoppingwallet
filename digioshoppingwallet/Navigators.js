import { createAppContainer } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import { Provider } from 'react-redux';
import HomeScreen from './HomeScreen';
import ProfileScreen from './Screen/ProfileScreen'
import RegisterScreen from './Screen/RegisterScreen';
import ScanPaymentScreen from './Screen/ScanPaymentScreen';
import PaymentScreen from './Screen/PaymentScreen';
import SlipScreen from './Screen/SlipScreen';
import WalletQRScreen from './Screen/WalletQRSceen';
import ScanTopupScreen from './Screen/ScanTopupScreen';
import TopupScreen from './Screen/TopupScreen';
import TransactionHistoryScreen from './Screen/TransactionHistoryScreen';
import CodePaymentScreen from './Screen/CodePaymentScreen';
import NotiScreen from './Screen/notiscreen';
import CodeTopupScreen from './Screen/CodeTopup';
import SecretKeyCheckScreen from './Screen/SecretkeyCheckScreen';
import TransactionDetailScreen from './Screen/TransactionDetailScreen';
import SettingScreen from './Screen/SettingScreen';

const AppNavigator = createStackNavigator({ 
  Home: {screen: HomeScreen,  navigationOptions: {
    header: null,
  },},
  Profile: {screen: ProfileScreen,navigationOptions: {
    header: null,
  },},
  Register:{screen:RegisterScreen},
  ScanPayment:{screen:ScanPaymentScreen,},
  Slip:{screen:SlipScreen,navigationOptions: {
    header: null,
  },},
  Payment:{screen:PaymentScreen,navigationOptions: {
    header: null,
  },},
  WalletQR:{screen:WalletQRScreen},
  ScanTopup:{screen:ScanTopupScreen},
  Topup:{screen:TopupScreen},
  TransactionHistory:{screen:TransactionHistoryScreen},
  CodePayment:{screen:CodePaymentScreen},
  Noti:{screen:NotiScreen},
  CodeTopup:{screen:CodeTopupScreen},
  SecretKeyCheck:{screen:SecretKeyCheckScreen,navigationOptions: {
    header: null,
  },},
  TransactionDetail:{screen:TransactionDetailScreen},
  Setting:{screen:SettingScreen},
});

const Navigators = createAppContainer(AppNavigator);

export default Navigators;