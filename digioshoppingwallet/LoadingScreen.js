import React from 'react'
import axios from 'axios';
import md5 from 'md5';
import { View, Text, StyleSheet,Alert} from 'react-native'
import { SkypeIndicator, WaveIndicator, BarIndicator,PacmanIndicator } from 'react-native-indicators';
import Button from 'apsl-react-native-button'
import { Assets } from 'react-navigation-stack';
import { fetchDataAll } from './actions/app';
import { storeUserData } from './actions/user';
import { connect } from 'react-redux';

console.disableYellowBox = true;
class LoadingScreen extends React.Component {

  state={
    isLoading: false
  }

  componentDidMount() {
    
  }
  componentWillReceiveProps(nextProps) {
    const { loading } = nextProps;
    console.log(loading.loading)
    this.setState({
      isLoading: loading.loading
    })
    
  }

 



  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ height:'100%', width:'100%', position: 'absolute', backgroundColor: 'white', opacity:0.8 }}>
          <PacmanIndicator color='blue' animating={true} size={80}/>
        </View>
      )
    } else {
      return (
        <View style={{ backgroundColor: 'red' }}/>
      )
    }
  }


}

const mapStateToProps = state => {
  return {
    loading: state.loading
  }
}

const mapDispatchToProps = {
}


export default connect(mapStateToProps, mapDispatchToProps)(LoadingScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});
