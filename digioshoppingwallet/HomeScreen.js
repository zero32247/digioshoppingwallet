import React from 'react'
import axios from 'axios';
import md5 from 'md5';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Keyboard, TouchableWithoutFeedback, Switch, AsyncStorage, Alert, Modal, Image, TouchableHighlight, ImageBackground } from 'react-native'
import Button from 'apsl-react-native-button'
import TopAlert from '@logisticinfotech/react-native-animated-alert';
import { storeUserData, userLogout, storePasswordData, storeBioAuthData } from './actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from './actions/loading';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { URLSERVICE, TIMEOUT } from './config/configvalue';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { copilot, walkthroughable, CopilotStep } from 'react-native-copilot';
import PropTypes from 'prop-types';
import * as LocalAuthentication from 'expo-local-authentication';
import { Dimensions } from "react-native";

console.disableYellowBox = true;
const CopilotText = walkthroughable(View);

const WalkthroughableText = walkthroughable(Text);
class HomeScreen extends React.Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };

  state = {
    User: [],
    username: '',
    password: '',
    expoPushToken: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    hidden: true,
    fontloading: false,
    secondStepActive: true,
    compatible: false,
    fingerprints: false,
    authenticated: false,
    modalVisible: true,
    failedCount: 0,
    fingerScanText: '',
    fingerbtnText: '',
  }

  static navigationOptions = {
    title: 'Home',
  };

  componentWillMount() {
    console.log('WILLMOUNT')
  }

  async componentDidMount() {
    console.log('DIDMOUNT')
    await Font.loadAsync({
      kanitLight: require('./assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true });
    const { user } = this.props;
    this.TestConnection();
    this.registerForPushNotificationsAsync();

    console.log("REDUX PERSIST:  " + user.username + "       " + user.bioAuth + "       " + user.password)
    if (user.username != undefined && user.password != undefined && user.bioAuth == true) {
      console.log("FINGER PRINT ACTIVES")
      this.setState({
        username: user.username,
        password: user.password
      })
    } else if (user.bioAuth == false || user.bioAuth == undefined) {
      console.log("SET MODAL VISBLE FALSE")
      this.setModalVisible(false);
    }
    //Keyboard.dismiss();
    //this._storeData();
  }

  CheckPersist() {
    this.checkDeviceForHardware();
    this.checkForFingerprints();
  }

  checkDeviceForHardware = async () => {
    let compatible = await LocalAuthentication.hasHardwareAsync();
    this.setState({ compatible });
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  clearState = () => {
    this.setState({ authenticated: false });
  };

  scanFingerPrint = async () => {
    Keyboard.dismiss();
    try {
      let results = await LocalAuthentication.authenticateAsync();
      console.log(results)
      if (results.success) {
        this.setState({
          modalVisible: false,
          authenticated: true,
          failedCount: 0,
        });
        this.Login()
      } else {
        console.log('Before Failed Count:   ' + this.state.failedCount)
        this.setState({
          failedCount: this.state.failedCount + 1,
          fingerScanText: 'Scan FingerPrint Fail Please TryAgain',
          fingerbtnText: 'Scan Again'
        });
        console.log('After Failed Count:   ' + this.state.failedCount)
        if (this.state.failedCount > 2) {
          this.setModalVisible(false)
          this.setState({
            alerttitle: 'Authentication',
            alertmessage: 'Finger Scan Failed Please Use Password',
            alertcolor: 'red'
          })
          TopAlert.showAlert();
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  handleStepChange = (step) => {
    //console.log(`Current step is: ${step.name}`);
  }

  componentWillReceiveProps(nextProps) {
    console.log('RECIEVEPROPS')
    const { storeUserData, dispatch, user } = nextProps;
    console.log("User" + user.username)

  }

  async TestConnection() {
    const { startloading, stoploading } = this.props;
    startloading();
    await axios.get(URLSERVICE + `user/start`, { timeout: 5000 })
      .then(res => {
        console.log("DATA:     " + res.data)
      }).catch(err => {
        console.log('------------------------------------');
        console.log("ConnectionLost:   " + err);
        stoploading();
        this.setState({
          alerttitle: 'NetWork',
          alertmessage: 'Cannot Connect to Server',
          alertcolor: 'red'
        })
        TopAlert.showAlert();
        console.log('------------------------------------');
      })
    stoploading();
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('username', 'admin');
      await AsyncStorage.setItem('password', '12345');
    } catch (error) {
      console.log("STOREDATEEROR:    " + error);
    }
  };

  Login() {
    const { storeUserData, startloading, stoploading, storePasswordData } = this.props;
    startloading()
    console.log('Login:    '+this.state.username+'     '+this.state.password+'      '+this.state.expoPushToken)
    axios({
      method: 'post',
      url: URLSERVICE + 'user/usernamecustomer',
      timeout: TIMEOUT,
      data: { username: this.state.username, password: this.state.password, token: this.state.expoPushToken }
    }).then(res => {
      //console.log(res.data)
      console.log("User:    " + res.data.User)
      if (res.data != '') {
        storeUserData(
          res.data.Customer.username,
          res.data.Customer.customerid,
          res.data.Customer.firstname,
          res.data.Customer.lastname,
          res.data.Customer.tel,
          res.data.Customer.birthday,
          res.data.Customer.walletid,
          res.data.Wallet.money,
          res.data.Wallet.secretkey,
          res.data.User.email,
          res.data.User.usertype,
          this.state.expoPushToken,
          res.data.User.logincount
        );
        storePasswordData(this.state.password);
        this.NaviToWallet();
      } else {
        stoploading();
        this.setState({
          alerttitle: 'Login',
          alertmessage: 'Username or Password Invalid',
          alertcolor: 'red'
        })
        TopAlert.showAlert();
      }
    }
    ).catch(err => {
      stoploading();
      console.log('ERROR:    ' + err)
      this.setState({
        alerttitle: 'Connection',
        alertmessage: 'Connection Timeout',
        alertcolor: 'red'
      })
      TopAlert.showAlert();
    })
  }

  registerForPushNotificationsAsync = async () => {
    console.log("REGISTERNOTI    WORKING")
    if (Constants.isDevice) {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS
        );
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync();
      //console.log("Token:         " + token);
      this.setState({ expoPushToken: token });
    } else {
      alert('Must use physical device for Push Notifications');
    }
  };

  NaviToWallet() {
    console.log("NAVIGATE WORK        55555555555555555555555555555555555555555555555555555555555555")
    const { navigate } = this.props.navigation;
    navigate('Profile');
  }

  NaviNoti() {
    const { navigate } = this.props.navigation;
    navigate('Noti');
  }
  onInputLabelPressed = () => {
    this.setState({ hidden: !this.state.hidden });
  };

  stop() {
    startloading()
    setTimeout(function () {

      stoploading()

    }, 1500);
  }
  dismiss(){
    Keyboard.dismiss();
    this.setModalVisible(false);
  }
  render() {
    const { navigate } = this.props.navigation;
    const { startloading, stoploading, storeBioAuthData, user } = this.props;
    const { fontloading } = this.state;
    const screenWidth = Math.round(Dimensions.get('window').width);

    if (fontloading) {
      return (
        <View>
          <TopAlert style={{ fontFamily: 'kanitLight', }} alertAnimatedIcon alertAutoHide alertTitle={this.state.alerttitle} alertMessage={this.state.alertmessage} alertBGColor={this.state.alertcolor} />
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>
            <TouchableWithoutFeedback onPress={() => this.dismiss()}>
              <View style={{ width: '100%', height: '100%' }}>
                <View style={{ height: '40%', justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: screenWidth / 9, color: '#fff' }}>DIGIO WALLET</Text>
                  <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: screenWidth / 35, color: '#fff' }}>Application for Digio Restaurant</Text>
                </View>
                <View style={{ width: '100%', height: '60%', justifyContent: 'center', alignItems: 'center' }}>
                  <View style={{ width: '90%', margin: '5%', padding: '5%', maxWidth: 400 }}>
                    <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>USERNAME</Text>
                    <TextInput autoFocus={true} onSubmitEditing={() => { this.secondTextInput.focus(); }} style={{ borderRadius: 20, fontFamily: 'kanitLight', color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 10, marginBottom: 10, paddingLeft: 20 }} onChangeText={(username) => this.setState({ username })} />
                    <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>PASSWORD</Text>
                    <View>
                      <TextInput secureTextEntry={this.state.hidden} ref={(input) => { this.secondTextInput = input; }} style={{ borderRadius: 20, fontFamily: 'kanitLight', color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 10, marginBottom: 10, paddingLeft: 20, paddingRight: 60 }} onChangeText={(password) => this.setState({ password: md5(password) })} />
                      <View style={{ width: '100%', height: 40, alignItems: 'flex-end', position: 'absolute' }}>
                        <TouchableOpacity style={{ width: 60, height: 40, marginTop: 10, justifyContent: 'center', alignItems: 'center', }} onPress={this.onInputLabelPressed}>
                          <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>
                            {this.state.hidden ? 'Show' : 'Hide'}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View style={{ marginTop: 5 }}>
                      <Button title="Login" style={{ borderRadius: 20, fontFamily: 'kanitLight', borderColor: '#fff', marginTop: 10 }} onPress={() => this.Login()} disabled={this.state.showprogress}>
                        <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>SIGN IN</Text></Button>

                      <Button title="Register" style={{ borderRadius: 20, fontFamily: 'kanitLight', borderColor: '#fff', marginTop: 10 }} onPress={() => navigate('Register')} disabled={this.state.showprogress}>
                        <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>CREATE AN ACCOUNT</Text></Button>
                    </View>
                  </View>
                </View>
                {/* FINGER PRINT */}
                <Modal animationType="slide" transparent={true} visible={this.state.modalVisible} onShow={this.scanFingerPrint}>
                  <View style={styles.modal}>
                    <View style={styles.innerContainer}>
                      <Text>Sign in with fingerprint</Text>
                      <Image style={{ width: 128, height: 128 }} source={require('./assets/fingerprint.png')} />
                      <TouchableHighlight
                        onPress={async () => {
                          this.dismiss();
                        }}>
                        <Text style={{ color: 'red', fontSize: 16 }}>Cancel</Text>
                      </TouchableHighlight>
                      {this.state.failedCount > 0 && (
                        <Text style={{ color: 'red', fontSize: 14 }}>
                          {this.state.fingerScanText}
                        </Text>
                      )}
                      <TouchableHighlight
                        onPress={async () => {
                          this.scanFingerPrint();
                        }}>
                        <Text style={{ color: 'red', fontSize: 16 }}>{this.state.fingerbtnText}</Text>
                      </TouchableHighlight>
                    </View>
                  </View>
                </Modal>
                {/* END FINGER PRINT */}
              </View>
            </TouchableWithoutFeedback>
          </LinearGradient>
          {this.stop()}
        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }
  // END RENDER
}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  storePasswordData,
  storeBioAuthData,
  startloading,
  stoploading,
  userLogout,
}

export default copilot({ animated: true })(connect(mapStateToProps, mapDispatchToProps)(HomeScreen));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    paddingTop: Constants.statusBarHeight,
    padding: 8,
  },
  modal: {
    flex: 1,
    marginTop: '90%',
    backgroundColor: '#E5E5E5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    marginTop: '30%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    alignSelf: 'center',
    fontSize: 22,
    paddingTop: 20,
  },
});