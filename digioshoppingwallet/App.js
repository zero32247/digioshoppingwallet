import React from 'react';
import { View, StyleSheet } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { Provider } from 'react-redux';
import Navigators from './Navigators';
import LoadingScreen from './LoadingScreen';

// import configureStore from './store'
// import { PersistGate } from 'redux-persist/integration/react';

// const { store, persister } = configureStore();
import configureStore from './store/index';
import { PersistGate } from 'redux-persist/integration/react';

const {store, persister} = configureStore();

export default class App extends React.Component {

  async componentDidMount() {
    // this.props.setCurrentLocation('pos');
  }


  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persister}>
          <View style={{ flex: 1 }}>
            <Navigators />
            {/* <View style={{ height:'100%', width:'100%', position: 'absolute'}}> */}
            <LoadingScreen />
            {/* </View> */}
          </View>
        </PersistGate>
      </Provider>
    );
  }
}






























/*import HomeScreen from './HomeScreen';
import ProfileScreen from './Screen/ProfileScreen'
import RegisterScreen from './Screen/RegisterScreen';
import ScanPaymentScreen from './Screen/ScanPaymentScreen';
import PaymentScreen from './Screen/PaymentScreen';
import SlipScreen from './Screen/SlipScreen';
import WalletQRScreen from './Screen/WalletQRSceen';
import ScanTopupScreen from './Screen/ScanTopupScreen';
import TopupScreen from './Screen/TopupScreen';
import TransactionHistoryScreen from './Screen/TransactionHistoryScreen';

const AppNavigator = createStackNavigator({
  Home: {screen: HomeScreen,  navigationOptions: {
    header: null,
  },},
  Profile: {screen: ProfileScreen},
  Register:{screen:RegisterScreen},
  ScanPayment:{screen:ScanPaymentScreen},
  Slip:{screen:SlipScreen},
  Payment:{screen:PaymentScreen},
  WalletQR:{screen:WalletQRScreen},
  ScanTopup:{screen:ScanTopupScreen},
});

const App = createAppContainer(AppNavigator);

export default App*/
