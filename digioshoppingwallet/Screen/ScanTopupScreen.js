import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { BackHandler, Alert } from 'react-native';
import { View, Text, StyleSheet, TextInput, TouchableWithoutFeedback, Image } from 'react-native';
import Button from 'apsl-react-native-button'
import { BarCodeScanner } from 'expo-barcode-scanner';
//import { Permissions } from 'expo-permissions';
import { Dimensions } from "react-native";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';

async function alertIfRemoteNotificationsDisabledAsync() {
  const { status } = await Permissions.getAsync(Permissions.BarCodeScanner);
  if (status !== 'granted') {
    alert('Hey! You might want to enable notifications for my app, they are good.');
  }
}

async function checkMultiPermissions() {
  const { status, expires, permissions } = await Permissions.getAsync(
    permissions.CAMERA,
  );
  if (status !== 'granted') {
    alert('Hey! You heve not enabled selected permissions');
  }
}


export default class ScanTopupScreen extends React.Component {

  static navigationOptions = {
    title: 'TopUp',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  UNSAFE_componentWillMount() {

  }

  componentWillMount() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    const screenHeight = Math.round(Dimensions.get('window').height);
    this.setState({ screenWidth: screenWidth, screenHeight: screenHeight })
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }



  async componentDidMount() {
    let { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermissionsGranted: (status === 'granted') });

  }



  render() {
    console.log(this.props)
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }


    return (
      <View style={{ height: '100%', width: '100%' }} >

        <View style={{ height: '100%', width: '100%' }}>
          <BarCodeScanner
            onBarCodeScanned={data => this.Getqrcodedata(data)}
            barCodeTypes={[
              BarCodeScanner.Constants.BarCodeType.qr,
              BarCodeScanner.Constants.BarCodeType.pdf417,
            ]}
            style={[StyleSheet.absoluteFill, styles.cameraContainer]}
            type={this.state.type}
          >
            <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}>
              <Image
                style={{ width: this.state.screenWidth * 0.6, height: this.state.screenWidth * 0.6 }}
                source={require('../assets/borderqr.png')}
              /></View>
          </BarCodeScanner>

        </View>
        {/* TOP */}
        <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
          <View style={{ position: 'absolute', width: '100%', height: 120 }}>
          <LinearGradient
                colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style={{ alignItems: 'center', width: '100%', height: 120, bottom: 0,paddingTop:80 }}>
            <TouchableWithoutFeedback onPress={() => this.CodeTopup()} disabled={this.state.showprogress}
             style={{ alignItems: 'center', width: '25%', height: 40, bottom: 0 }}>
                <View style={{ width: '25%', height: 40 }}>
                  <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff', height: 40,borderBottomWidth:5,borderColor:'#399cca' }}>SCAN</Text>
                </View>
            </TouchableWithoutFeedback>
            </LinearGradient>
          </View>
        </View>
        {/* BOTTOM */}
        <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
          <View style={{ bottom: 0, position: 'absolute', width: '100%', height: 120 }}>
          <LinearGradient
                colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style={{ alignItems: 'center', width: '100%', height: '100%', bottom: 0, justifyContent: 'center' }}>
              
                <View style={{width: 250 , height:40 , justifyContent: 'center' }}>
                <LinearGradient
                colors={['#44b6ec', '#3494c1', '#2081af']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style={{ width: '100%', height: '100%',borderRadius:20,paddingTop:5 }}>
                  <TouchableWithoutFeedback onPress={() => this.CodeTopup()} disabled={this.state.showprogress}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>CODE TOPUP</Text>
                  </TouchableWithoutFeedback>
                  </LinearGradient>
                </View>
            
            </LinearGradient>
          </View>
        </View>

      </View>
    )
  }

  CodeTopup() {
    console.log("PRESS")
    const { navigate } = this.props.navigation;
    navigate('CodeTopup');
  }

  Getqrcodedata(data) {
    const input = data.data;
    const [username, customerid, walletid] = input.split(',');
    //this.setState({ customerid: customerid,walletid:walletid,expoPushToken:expoPushToken })
    this.onScannedComplete(username, customerid, walletid);
    //console.log("CUSTOMERID    " + this.state.customerid + "     WALLETID   " + this.state.walletid)
  }

  onScannedComplete(username, customerid, walletid) {

    if ((username != '') == true && (walletid != '') == true && (customerid != '') == true) {
      console.log('------------------------------------');
      console.log("TRUE");
      console.log('------------------------------------');
      axios.get(URLSERVICE + `customer/id/` + customerid, { timeout: TIMEOUT })
        .then(res => {
          console.log(res.data);
          if (res.data != '') {
            const { navigate } = this.props.navigation;
            navigate('Topup', {
              username: res.data.username,
              customerid: customerid,
              walletid: walletid,
              firstname: res.data.firstname,
              lastname: res.data.lastname
            });
          }
        })
    } else {
      console.log('------------------------------------');
      console.log("FALSE");
      console.log('------------------------------------');
    }

  }


}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});