import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, Image, Modal, TouchableHighlight, Alert,useEffect, useRef, useState,TouchableOpacity } from 'react-native'
import Button from 'apsl-react-native-button'
import { SkypeIndicator, WaveIndicator, BarIndicator } from 'react-native-indicators';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { storeUserData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import TopAlert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { copilot, walkthroughable, CopilotStep } from 'react-native-copilot';
import PropTypes from 'prop-types';
import Constants from 'expo-constants';
import * as LocalAuthentication from 'expo-local-authentication';



class SecretKeyCheckScreen extends React.Component {
  static navigationOptions = {
    title: 'SecretKeyCheck',
  };
  state = {
    username: '',
    customerid: '',
    walletid: '',
    type: '',
    merchantid: '',
    merchantname: '',
    orderid: '',
    total: '',
    money: '',
    secrectkey: '',
    password: '',
    merchantwalletid: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    amount: '',
    expoPushToken: '',
    firstname: '',
    lastname: '',
    pin: '',
    fontloading: false,
    compatible: false,
    fingerprints: false,
    authenticated: false,
    modalVisible: false,
    failedCount: 0,
    fingerScanText: '',
    fingerbtnText: '',
    key:''
  }

  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })
    const type = this.props.navigation.state.params.type;
    console.log("TYPE:    " + type)
    if (type == 'Slip') {
      const { storeUserData, dispatch, user, startloading, stoploading } = this.props;
      const merchantid = this.props.navigation.state.params.merchantid;
      const merchantname = this.props.navigation.state.params.merchantname;
      const orderid = this.props.navigation.state.params.orderid;
      const total = this.props.navigation.state.params.total;
      const merchantwalletid = this.props.navigation.state.params.merchantwalletid;

      console.log("" + user.customerid + "    " + user.walletid + "   " + user.money + "   " + user.secretkey + "   " + user.username)
      this.setState({
        customerid: user.customerid,
        walletid: user.walletid,
        money: user.money,
        secretkey: user.secretkey,
        merchantid: merchantid,
        merchantname: merchantname,
        orderid: orderid,
        total: total,
        merchantwalletid: merchantwalletid,
        username: user.username,
        type: type,
      })
    } else if (type == 'Profile') {
      const username = this.props.navigation.state.params.username;
      const walletid = this.props.navigation.state.params.walletid;
      const customerid = this.props.navigation.state.params.customerid;
      const firstname = this.props.navigation.state.params.firstname;
      const lastname = this.props.navigation.state.params.lastname;
      const amount = this.props.navigation.state.params.amount;
      //const expoPushToken = this.props.navigation.state.params.expoPushToken;
      const { user } = this.props;
      this.setState({
        secretkey: user.secretkey,
        username: username,
        customerid: customerid,
        walletid: walletid,
        firstname: firstname,
        lastname: lastname,
        amount: amount,
        type: type,
      })
      console.log("Secretkey" + user.secretkey)
      this.getExpoToken(username);
    }
    const { startloading, stoploading, user } = this.props;
    stoploading();

    if (user.bioAuth == true) {
      this.setModalVisible(true);
    }

  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  clearState = () => {
    this.setState({ authenticated: false });
  };

  scanFingerPrint = async () => {

    try {
      let results = await LocalAuthentication.authenticateAsync();
      if (results.success) {
        this.setState({
          modalVisible: false,
          authenticated: true,
          failedCount: 0,
          fingerScanText: 'Authentication Confirm'
        });
        this.CheckTimeout();
      } else {
        console.log('Before Failed Count:   ' + this.state.failedCount)
        this.setState({
          failedCount: this.state.failedCount + 1,
          fingerScanText: 'Scan FingerPrint Fail Please TryAgain',
          fingerbtnText: 'Scan Again'
        });
        console.log('After Failed Count:   ' + this.state.failedCount)
      }
    } catch (e) {
      console.log(e);
    }
  };

  onComplete(pin) {
    const { user } = this.props;
    const [num1] = pin.split(',');
    const pass = num1
    const passencypt = md5(pin)
    console.log("pin:    " + pin)
    console.log("passencypt:    " + passencypt)
    
    console.log("ENCRYPT:    " + 
    user.secretkey + "       " + passencypt + "     " + (passencypt == this.state.secrectkey))
    if (passencypt == user.secretkey) {
      this.CheckTimeout();
    } else {
      //clear();
      this.setState({ key: '' });
      stoploading();
      this.setState({
        alerttitle: 'SecretKey',
        alertmessage: 'Incorrect SecretKey',
        alertcolor: 'red'
      })
      TopAlert.showAlert();
    }
  }

  CheckTimeout() {
    const { user, startloading, stoploading } = this.props;
    axios.get(URLSERVICE + 'user/timeout/' + user.username)
      .then(res => {
        console.log("TIMEOUT RES:  " + res.data)
        if (res.data == 'timeout') {
          stoploading();
          console.log("TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT");
          Alert.alert(
            'Timeout',
            'No Activity Toolong Please Login Again',
            [
              { text: 'Confrim', onPress: () => this.Home() },
            ],
            { cancelable: false }
          );
        } else {
          this.CheckType();
        }
      }).catch(err => {
        stoploading();
        console.log("TIMEOUT ERROR:   " + err)
      })
  }


  Home() {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  CheckType() {
    console.log("CHECKTYPE:    " + this.state.type)
    if (this.state.type == 'Slip') {
      this.onPayment();
    } else if (this.state.type == 'Profile') {
      this.TopUp();
    }
  }


  /* Payment */

  onPayment() {
    const { startloading, stoploading } = this.props;
    startloading();
    if (this.state.money >= this.state.total) {
      console.log("MoneyPass")
      this.CreateTransaction();
    } else {
      this.setState({
        alerttitle: 'Payment',
        alertmessage: 'Not Enough Money',
        alertcolor: 'orange'
      })
      TopAlert.showAlert();
      stoploading();
      const { navigate } = this.props.navigation;
      setTimeout(() => {
        navigate('Profile');
      }, 2000);

    }
  }

  CreateTransaction() {
    const { storeUserData, dispatch, user, startloading, stoploading } = this.props;
    console.log("CreateTransaction:   " + this.state.orderid)
    const millsec = Math.round((new Date()).getTime());
    //console.log('NOW:   '+millsec);
    axios({
      method: 'post',
      url: URLSERVICE + 'transaction/pay/' + this.state.merchantwalletid,
      timeout: 30000,
      data: { customerid: this.state.customerid, transactiontype: 'Pay', amount: this.state.total, transactiontime: millsec, orderid: this.state.orderid }
    }).then(res => {
      //console.log(res);
      const { navigate } = this.props.navigation;
      stoploading();
      navigate('Slip', { money: this.state.money, merchantname: this.state.merchantname, total: this.state.total, SlipType: 'Payment' });
    }
    ).catch(err => {
      stoploading();
      console.log(err);
      this.setState({
        alerttitle: 'Payment',
        alertmessage: 'Payment Not Successful',
        alertcolor: 'red'
      })
      TopAlert.showAlert();
    })
  }


  /* TopUp */
  getExpoToken(username) {
    const { startloading, stoploading } = this.props;
    startloading();
    console.log("Usernametopup:     " + username)
    axios.get(URLSERVICE + `user/token/` + username)
      .then(res => {
        console.log(res.data)
        const expotoken = res.data;
        this.setState({
          expoPushToken: expotoken
        })
        stoploading();
      }).catch(err => {
        console.log("GetExpoToken Error:   " + err)
        stoploading();
      })
  }


  TopUp() {
    const { startloading, stoploading } = this.props;
    const { user } = this.props;
    console.log("CreateTransaction")
    const millsec = Math.round((new Date()).getTime());
    //console.log('NOW:   '+millsec);
    startloading();
    axios({
      method: 'post',
      url: URLSERVICE + 'transaction/add/' + this.state.customerid,
      data: { customerid: user.customerid, transactiontype: 'TopUp', amount: this.state.amount, transactiontime: millsec, orderid: '' }
    }).then(res => {
      //console.log(res);
      this.topupcomplete();
    }
    ).catch(err => {
      stoploading();
      console.log("TopUpError:    " + err)
      this.setState({
        alerttitle: 'TopUp',
        alertmessage: 'Topup Error',
        alertcolor: 'red'
      })
      TopAlert.showAlert();
      const { navigate } = this.props.navigation;
      //navigate('Profile');
    })
  }

  async topupcomplete() {
    const { startloading, stoploading } = this.props;
    stoploading();
    this.setState({
      alerttitle: 'Topup',
      alertmessage: 'Topup Complete',
      alertcolor: 'green'
    })
    TopAlert.showAlert();
    this.sendPushNotification();
    const { navigate } = await this.props.navigation;
    setTimeout(() => {
      navigate('Profile');
    }, 3000);
  }


  sendPushNotification = async () => {
    console.log('------------------------------------');
    console.log("EXPOTOKEN:    " + this.state.expoPushToken);
    console.log('------------------------------------');
    const message = {
      to: this.state.expoPushToken,
      sound: 'default',
      title: 'TopUp Complete',
      body: 'TopUp:  ' + this.state.amount + ' Baht',
      data: { data: 'TopupComplete' },
    };
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
    const data = response._bodyInit;
    console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
  };



  setkey(num) {
    console.log('num :: '+num);
    console.log('length :: '+this.state.key.length);
    const number = this.state.key;
    const number2 = number + num;
    if (number.length < 6) {
      this.setState({ key: number + num });
  }
  if(number2.length == 6){
    
    startloading();
    this.onComplete(number2);
  }
  
}
  render() {
    const { startloading, stoploading } = this.props;
    const { password } = this.state;
    //console.log(this.props)
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%', height: '7%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text></View>
            </View>
            <View style={{ width: '100%', height: '13%', justifyContent: "center" }}>
              <View>
                <View>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>ENTER PIN</Text>
                </View>
              </View>
              <View style={{ height: 20 }}><Text style={{
                fontFamily: 'kanitLight', fontSize: 18

                , color: '#ccc', textAlign: 'center'
              }}>Enter Secret Pin here</Text></View>
            </View>
            <View style={{ width: '100%', height: '87%', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
              <View style={{ alignItems: 'center' }}>
                <View style={{ height: "80%" }}>
                  <View style={{ alignItems: 'center', width: '100%', maxWidth: 400, padding: 5 }} >
                    <View style={{ alignItems: 'center', width: '100%' }}>
                    
                    <View style={{ alignItems: 'center' }}>ุ
                                <SmoothPinCodeInput password mask="﹡"
                                    ref={(input) => { this.skey = input; }}
                                    cellStyle={{ borderRadius:100 , borderColor: 'gray', borderWidth: 1, marginTop: 5 }}
                                    cellStyleFocused={{
                                        borderColor: 'black',
                                    }}
                                    textStyle={{
                                        fontSize: 24,
                                      color: '#fff'
                                    }}
                                    codeLength={6}
                                    value={this.state.key}
                                    onPress={() => Keyboard.dismiss()}
                                    onFocus={() => Keyboard.dismiss()}
                                />
                            </View>

                            <View style={{ alignItems: 'center', width: '100%' }}>
                                <View style={{ alignItems: 'center', width: '100%', maxWidth: 400, padding: 20 }} >
                                    <View style={{ alignItems: 'center', width: 240 }}>

                                        <View style={{ left: 0, width: 80, position: 'absolute' }}>
                                            {/* 1 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('1')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff', fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>1</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 4 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('4')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>4</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 7 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('7')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>7</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/*  */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }}>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ alignItems: 'center', width: 80, position: 'absolute' }}>
                                            {/* 2 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('2')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>2</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 5 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('5')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>5</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 8 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('8')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>8</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 0 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('0')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>0</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ right: 0, width: 80, position: 'absolute' }}>
                                            {/* 3 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('3')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>3</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 6 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('6')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>6</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 9 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('9')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>9</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* < */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setState({ key: this.state.key.substring(0, this.state.key.length - 1) })}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Image
                                                        style={{ width: 30, height: 30, margin: 15,color:'#fff' }}
                                                        source={require('../assets/ui.png')}
                                                    />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                      
                      {/* <PinView
                        onComplete={(pin,clear) => this.onComplete(pin,clear)}
                        pinLength={6}
                        inputActiveBgColor='#fff'
                        buttonActiveOpacity={0.5}
                        buttonBgColor='rgba(255,255,255,0.2)'
                        buttonTextColor='#fff'
                      /> */}

                    </View>
                  </View>
                </View>
              </View>
            </View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onShow={this.scanFingerPrint}>
              <View style={styles.modal}>
                <View style={styles.innerContainer}>
                  <Text>Sign in with fingerprint</Text>
                  <Image
                    style={{ width: 128, height: 128 }}
                    source={require('../assets/fingerprint.png')}
                  /><TouchableHighlight
                  onPress={async () => {
                    this.setModalVisible(false);
                  }}>
                  <Text style={{ color: 'red', fontSize: 16 }}>Cancel</Text>
                </TouchableHighlight>
                  {this.state.failedCount > 0 && (
                    <Text style={{ color: 'red', fontSize: 14 }}>
                      {this.state.fingerScanText}
                    </Text>

                  )}
                  <TouchableHighlight
                    onPress={async () => {
                      this.scanFingerPrint();
                    }}>
                    <Text style={{ color: 'red', fontSize: 16 }}>{this.state.fingerbtnText}</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </Modal>
          </LinearGradient>
          <TopAlert
            alertAnimatedIcon
            alertAutoHide
            alertTitle={this.state.alerttitle}
            alertMessage={this.state.alertmessage}
            alertBGColor={this.state.alertcolor}
          />
          {stoploading()}
        </View>
      )
      // return (
      //   <View>
      //     <LinearGradient
      //       colors={['#192f6a', '#3b5998', '#4c669f']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
      //       style={{ alignItems: 'center', height: "100%" }}>
      //       <View style={{ width: '100%', height: '100%', padding: 10 }}>
      //         <View style={{ paddingTop: 30, width: '100%' }}>
      //           <Text style={{ fontFamily: 'kanitLight', fontSize: 30, textAlign: 'center', color: '#fff' }}>Enter Pin</Text>
      //           <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'center', color: '#fff' }}>Enter Secret Pin here</Text>
      //         </View>
      //         <View style={{ paddingTop: 20 }}>
      //           <PinView
      //             onPress={(pin) => console.log("PIN:   " + parseInt(pin.toString()))}
      //             onComplete={(pin, clear) => this.onComplete(pin, clear)}
      //             pinLength={6}
      //             inputActiveBgColor='#fff'
      //             buttonActiveOpacity={0.5}
      //             buttonBgColor='rgba(255,255,255,0.2)'
      //             buttonTextColor='#fff'
      //           /></View>
      //       </View>
      //       <Modal
      //         animationType="slide"
      //         transparent={true}
      //         visible={this.state.modalVisible}
      //         onShow={this.scanFingerPrint}>
      //         <View style={styles.modal}>
      //           <View style={styles.innerContainer}>
      //             <Text>Sign in with fingerprint</Text>
      //             <Image
      //               style={{ width: 128, height: 128 }}
      //               source={require('../assets/fingerprint.png')}
      //             />
      //             {this.state.failedCount > 0 && (
      //               <Text style={{ color: 'red', fontSize: 14 }}>
      //                 {this.state.fingerScanText}
      //               </Text>

      //             )}
      //             <TouchableHighlight
      //               onPress={async () => {
      //                 this.scanFingerPrint();
      //               }}>
      //               <Text style={{ color: 'red', fontSize: 16 }}>{this.state.fingerbtnText}</Text>
      //             </TouchableHighlight>
      //           </View>
      //         </View>
      //       </Modal>


      //     </LinearGradient><TopAlert
      //       alertAnimatedIcon
      //       alertAutoHide
      //       alertTitle={this.state.alerttitle}
      //       alertMessage={this.state.alertmessage}
      //       alertBGColor={this.state.alertcolor}
      //     />
      //   </View>
      // )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render

}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  startloading,
  stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(SecretKeyCheckScreen);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    paddingTop: Constants.statusBarHeight,
    padding: 8,
  },
  modal: {
    flex: 1,
    marginTop: '90%',
    backgroundColor: '#E5E5E5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    marginTop: '30%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    alignSelf: 'center',
    fontSize: 22,
    paddingTop: 20,
  },
});