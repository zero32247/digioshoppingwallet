import React from 'react';
import { Text, View, Button, Vibration, Platform, StyleSheet, Image, TouchableHighlight, Switch, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { storeUserData, storeBioAuthData, userLogout } from '../actions/user';
import TopAlert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as LocalAuthentication from 'expo-local-authentication';


class SettingScreen extends React.Component {

  static navigationOptions = {
    title: 'Setting',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  state = {
    value: false,
    compatible: false,
    compatibleshow: false,
  }

  componentDidMount() {
    const { user } = this.props;
    this.setState({ value: user.bioAuth });
    this.checkDeviceForHardware();
  }

  checkDeviceForHardware = async () => {
    let compatible = await LocalAuthentication.hasHardwareAsync();
    this.setState({ compatible });
  };


  SetBioAuthData(bioAuth) {
    const { storeBioAuthData } = this.props;
    if (this.state.compatible == true) {
      this.setState({ value: bioAuth })
      storeBioAuthData(bioAuth)
    } else if (this.state.compatible == false) {
      this.setState({ compatibleshow: true });
    }

  }

  Logout() {
    const { storeBioAuthData } = this.props;
    storeBioAuthData(false);
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  render() {

    return (
      <View>
        <LinearGradient
          colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
          style={{ alignItems: 'center', height: "100%" }}>
          <View style={{ width: '100%', height: 80, borderBottomWidth: 1, borderColor: '#999' }}></View>

          {/* Finger Print Login */}

          <View style={{ width: '100%', height: 50, borderBottomWidth: 1, borderColor: '#999', justifyContent: "center" }}>
            <View style={{ width: '50%', paddingLeft: 10 }}>
              <Text style={{ fontSize: 15, color: '#fff' }}>
                Finger Print Login 
              </Text> 
            </View>
            <View style={{ width: '50%', position: 'absolute', right: 0, paddingRight: 10 }}><Switch value={this.state.value} disabled={this.state.compatibleshow} onValueChange={value => { this.SetBioAuthData(value) }} /></View>
          </View>


          {/* BOTTOM */}
          <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
            <View style={{ bottom: 0, position: 'absolute', width: '100%', height: 60, alignItems: 'center' }}>

              <View style={{ width: 250, height: 40, justifyContent: 'center' }}>
                <LinearGradient
                  colors={['#44b6ec', '#3494c1', '#2081af']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                  style={{ width: '100%', height: '100%', borderRadius: 20, paddingTop: 5 }}>
                  <TouchableWithoutFeedback onPress={() => this.Logout()} disabled={this.state.showprogress}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>LOGOUT</Text>
                  </TouchableWithoutFeedback>
                </LinearGradient>

              </View>
            </View>
          </View>
        </LinearGradient>
      </View>
    );

  }
}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  storeBioAuthData,
  userLogout,
}


export default connect(mapStateToProps, mapDispatchToProps)(SettingScreen);