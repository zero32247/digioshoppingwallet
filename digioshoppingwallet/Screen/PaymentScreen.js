import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, TextInput, ActivityIndicator, ImageBackground, Vibration, TouchableWithoutFeedback, Alert } from 'react-native'
import { SkypeIndicator, WaveIndicator, BarIndicator } from 'react-native-indicators';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { storeUserData, storeTutorialData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import Button from 'apsl-react-native-button'
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { copilot, walkthroughable, CopilotStep } from '@okgrow/react-native-copilot';
import PropTypes from 'prop-types';


const CopilotView = walkthroughable(View, Text);
class PaymentScreen extends React.Component {

  static navigationOptions = {
    title: 'Payment',
  };
  state = {
    customers: [],
    showprogress: false,
    username: '',
    merchantid: '',
    merchantname: '',
    orderid: '',
    total: '',
    customerid: '',
    walletid: '',
    money: '',
    secrectkey: '',
    password: '',
    merchantwalletid: '',
    fontloading: false,
    secondStepActive: true,
  }
  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })
    const { startloading, stoploading, user } = this.props;
    stoploading();

    if (user.logincount.includes('ScreenPayment') == false) {
      this.props.copilotEvents.on('stepChange', this.handleStepChange);
      this.props.start();
    }

    this.props.copilotEvents.on('stop', () => {
      this.UpdateTutorial();
    });
  }

  UpdateTutorial() {
    const { storeTutorialData } = this.props;
    axios({
      method: 'post',
      url: URLSERVICE + 'user/tooltip/' + this.state.username,
      timeout: 30000,
      data: "ScreenPayment"
    }).then(res => {
      console.log("Update Tutorail Complete");
      storeTutorialData(this.state.username);
    }
    )
  }

  handleStepChange = (step) => {
    console.log(`Current step is: ${step.name}`);
  }



  UNSAFE_componentWillMount() {
    const { user } = this.props;
    const merchantid = this.props.navigation.state.params.merchantid;
    const merchantname = this.props.navigation.state.params.merchantname;
    const orderid = this.props.navigation.state.params.orderid;
    const total = this.props.navigation.state.params.total;
    const merchantwalletid = this.props.navigation.state.params.merchantwalletid;
    console.log("MerchantData:     " + merchantid + "    " + merchantname + "    " + orderid + "    " + total + "     " + merchantwalletid)
    console.log("" + user.customerid + "    " + user.walletid + "   " + user.money + "   " + user.secretkey + "   " + user.username)
    this.setState({
      customerid: user.customerid,
      walletid: user.walletid,
      money: user.money,
      secretkey: user.secretkey,
      merchantid: merchantid,
      merchantname: merchantname,
      orderid: orderid,
      total: total,
      merchantwalletid: merchantwalletid,
      username: user.username
    })
  }

  CheckTimeout() {
    const { user, startloading, stoploading } = this.props;
    startloading();
    axios.get(URLSERVICE + 'user/timeout/' + user.username)
      .then(res => {
        console.log("TIMEOUT RES:  " + res.data)
        if (res.data == 'timeout') {
          stoploading();
          console.log("TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT");
          Alert.alert(
            'Timeout',
            'No Activity Toolong Please Login Again',
            [
              { text: 'Confrim', onPress: () => this.Home() },
            ],
            { cancelable: false }
          );
        } else {
          this.checkSecretkey();
        }
      }).catch(err => {
        stoploading();
        console.log("TIMEOUT ERROR:   " + err)
      })
  }

  Home() {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }


  checkSecretkey() {
    const { navigate } = this.props.navigation;
    navigate('SecretKeyCheck', {
      merchantid: this.state.merchantid,
      merchantname: this.state.merchantname,
      merchantwalletid: this.state.merchantwalletid,
      orderid: this.state.orderid,
      total: this.state.total,
      type: 'Slip',
    });
  }

  Home() {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  render() {
    const { startloading, stoploading } = this.props;
    const { password } = this.state;
    //console.log(this.props)
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%', height: '10%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text></View>
            </View>
            <View style={{ width: '100%', height: '20%', justifyContent: "center" }}>
              <View>
                <View>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>PAYMENT</Text>
                </View>
              </View>
              <View style={{ height: 20 }}><Text style={{
                fontFamily: 'kanitLight', fontSize: 13

                , color: '#777', textAlign: 'center'
              }}>BALANCE</Text></View>
            </View>
            <View style={{ width: '100%', height: '70%', paddingBottom: 10, paddingTop: 20, backgroundColor: '#fff', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
            <CopilotStep text="Payment Detail" order={1} name="hello1">
              <CopilotView>
                <View style={{ textAlign: 'center' }}>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'center', color: '#222', textTransform: 'uppercase' }}>To : {this.state.merchantname}</Text>
                </View>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textAlign: 'center', color: '#222' }}>MERCHANT NO. {this.state.merchantwalletid}</Text>
                <View style={{ textAlign: 'center', paddingTop: 50 }}>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 17, textAlign: 'center', color: '#222', textTransform: 'uppercase' }}>Total (THB)</Text>
                </View>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 50, textAlign: 'center', color: '#222' }}>{this.state.total} ฿</Text>
              </CopilotView>
            </CopilotStep>
            </View>
            {/* BOTTOM */}
            <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
              <View style={{ bottom: 0, position: 'absolute', width: '100%', height: 60, alignItems: 'center' }}>
                  
                  <View style={{ width: 250, height: 40, justifyContent: 'center' }}>
                  <CopilotStep text="Scan QrCode Here" order={2} name="hello2">
                <CopilotView style={{ bottom: 0, position: 'absolute', width: '100%', alignItems: 'center', height: 50, bottom: 0, justifyContent: 'center', backgroundColor: '#fff' }}>
                    <LinearGradient
                      colors={['#44b6ec', '#3494c1', '#2081af']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                      style={{ width: '100%', height: '100%', borderRadius: 20, paddingTop: 5 }}>
                      <TouchableWithoutFeedback onPress={() => this.CheckTimeout()} disabled={this.state.showprogress}>
                        <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>CONFIRM</Text>
                      </TouchableWithoutFeedback>
                    </LinearGradient>
                </CopilotView>
              </CopilotStep>
                  </View>
              </View>
            </View>
          </LinearGradient>
        </View>
      )
      // return (
      //   <View style={{ height: '100%', width: '100%' }} >
      //     <LinearGradient
      //       colors={['#192f6a', '#3b5998', '#4c669f']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
      //     >
      //       <View style={{ height: '100%', width: '100%', paddingTop: 50, paddingLeft: 20, paddingRight: 20 }}>

      //         <CopilotStep text="Scan QrCode Here" order={1} name="hello1">
      //           <CopilotView style={{ height: '60%', width: '100%'}}>
      //             <View style={{backgroundColor: '#fff', borderRadius: 10, padding: 20, height: '100%' }}>
      //               <View style={{ width: '100%', height: 50 }}>
      //                 <Text style={{ fontFamily: 'kanitLight', fontSize: 30, textAlign: 'center' }}>Payment</Text>

      //               </View>

      //               <View style={{ width: '30%', height: 50 }}>
      //                 <Text style={{ fontFamily: 'kanitLight', fontSize: 15 }}>To : {this.state.merchantname}</Text>
      //                 <Text style={{ fontFamily: 'kanitLight', fontSize: 15, marginTop: 10 }}>{('00000000' + this.state.merchantwalletid).substring(('00000000' + this.state.merchantwalletid).length - 6, ('00000000' + this.state.merchantwalletid).length)}</Text></View>


      //               <View style={{ width: '100%', paddingTop: 50, paddingBottom: 50 }}>
      //                 <View style={{ width: '100%', height: 50 }}>
      //                   <Text style={{ fontFamily: 'kanitLight', fontSize: 15 }}>Total (THB)</Text>
      //                   <Text style={{ fontFamily: 'kanitLight', fontSize: 30, textAlign: 'right' }}>{this.state.total}</Text>
      //                 </View>

      //               </View>
      //               </View>
      //             </CopilotView>
      //           </CopilotStep>
      //       </View>
      //         <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
      //           <CopilotStep text="Scan QrCode Here" order={2} name="hello2">
      //             <CopilotView style={{ bottom: 0, position: 'absolute', width: '100%', alignItems: 'center', height: 50, bottom: 0, justifyContent: 'center', backgroundColor: '#fff' }}><TouchableWithoutFeedback onPress={() => this.CheckTimeout()} disabled={this.state.showprogress}>
      //               <View style={{ width: '100%', justifyContent: 'center', }}>
      //                 <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#222' }}>Confirm</Text>
      //               </View>
      //             </TouchableWithoutFeedback>
      //             </CopilotView>
      //           </CopilotStep>
      //         </View>

      //     </LinearGradient>
      //   </View>

      //     )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render

}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  storeTutorialData,
  startloading,
  stoploading
}

export default copilot({ verticalOffset: 0, horizontalOffset: 0 })(connect(mapStateToProps, mapDispatchToProps)(PaymentScreen));



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});