import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Keyboard, TouchableWithoutFeedback, Image } from 'react-native'
import Button from 'apsl-react-native-button'
import DatePicker from 'react-native-datepicker';
import { Permissions } from 'expo-permissions';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import { Dimensions } from "react-native";
import Alert from '@logisticinfotech/react-native-animated-alert';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { startloading, stoploading } from '../actions/loading';
import { connect } from 'react-redux';

async function alertIfRemoteNotificationsDisabledAsync() {
  const { status } = await Permissions.getAsync(Permissions.CALENDAR);
  if (status !== 'granted') {
    alert('Hey! You might want to enable notifications for my app, they are good.');
  }
}

async function checkMultiPermissions() {
  const { status, expires, permissions } = await Permissions.getAsync(
    Permissions.CALENDAR,
  );
  if (status !== 'granted') {
    alert('Hey! You heve not enabled selected permissions');
  }
}



class RegisterScreen extends React.Component {
  static navigationOptions = {
    title: 'CREATE YOUR ACCOUNT',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  state = {
    username: '',
    password: '',
    firstname: '',
    lastname: '',
    email: '',
    tel: '',
    birthday: '',
    secretkey: '',
    date: '',
    hidden: true,
    fontloading: false
  }

  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })


    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();

    this.setState({

      maxday:
        year + "-" + month + "-" + date,
    });
    const { startloading, stoploading } = this.props;
    stoploading();
  }
  componentWillMount() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    const screenHeight = Math.round(Dimensions.get('window').height);
    this.setState({ screenWidth: screenWidth, screenHeight: screenHeight })
  }
  validate = (text) => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text })
      this.setState({ checkemail: 'false' })
      return false;
    }
    else {
      this.setState({ email: text })
      this.setState({ checkemail: 'true' })
      console.log("Email is Correct");
    }
  }

  onInputLabelPressed = () => {
    this.setState({ hidden: !this.state.hidden });
  };
  render() {
    const { startloading, stoploading } = this.props;
    const screenWidth = Math.round(Dimensions.get('window').width);
    //console.log(this.props)
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View style={{ width: '100%', height: '100%' }}>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

          <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
            <View style={{ width: '100%', padding: 15 }}>
              <View style={{ height: '20%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: screenWidth / 15, color: '#fff' }}>CREATE YOUR ACCOUNT</Text>
              </View>
              <View style={{ height: '80%', justifyContent: 'center', alignItems: 'center' }}>

                <View style={{ width: '94%', marginLeft: '3%', marginRight:'3%', marginBottom:0, paddingTop: '2%', paddingLeft: '2%', paddingRight: '2%', maxWidth: 600 }}>

                  <View>
                    <View style={{ width: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Firstname</Text>
                      <TextInput autoFocus={true} onSubmitEditing={() => { this.lastname.focus(); }} style={{ fontFamily: 'kanitLight',borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(firstname) => this.setState({ firstname })} value={this.state.firstname} />
                    </View>
                    <View style={{ width: '50%', position: 'absolute', marginLeft: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Lastname</Text>
                      <TextInput ref={(input) => { this.lastname = input; }} onSubmitEditing={() => { this.tel.focus(); }} style={{ fontFamily: 'kanitLight',borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(lastname) => this.setState({ lastname })} value={this.state.lastname} />
                    </View>
                  </View>
                  <View>
                    <View style={{ width: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Tel</Text>
                      <TextInput ref={(input) => { this.tel = input; }} keyboardType='numeric' maxLength={10} style={{fontFamily: 'kanitLight', borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(tel) => this.setState({ tel })} value={this.state.tel} />
                    </View>
                    <View style={{ width: '50%', position: 'absolute', marginLeft: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>BirthDay</Text>
                      <DatePicker
                        style={{ width: '100%', borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }}
                        date={this.state.date}
                        mode="date"
                        placeholder="select date"
                        format="YYYY-MM-DD"
                        minDate="1900-01-01"
                        maxDate={this.state.maxday}

                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 2,
                            marginLeft: -15
                          },
                          dateInput: {
                            marginLeft: 0,
                            borderColor: 'rgba(0,0,0,0)'
                          },
                  placeholderText: {fontFamily: 'kanitLight',
                    color: '#fff'
                },
                dateText:{fontFamily: 'kanitLight',
                  color: '#fff',
                }
                        }}
                        onDateChange={(date) => { this.setState({ date: date }), this.email.focus() }}
                      /></View>
                  </View>
                  <View style={{ width: '100%', padding: 3 }}>
                    <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Email</Text>
                    <TextInput ref={(input) => { this.email = input; }} onSubmitEditing={() => { this.username.focus(); }} style={{ fontFamily: 'kanitLight',borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(text) => this.validate(text)}
                      value={this.state.email} />
                  </View>

                  <View>
                    <View style={{ width: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Username</Text>
                      <TextInput ref={(input) => { this.username = input; }} onSubmitEditing={() => { this.password.focus(); }} style={{ fontFamily: 'kanitLight',borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(username) => this.setState({ username })} value={this.state.username} />
                    </View>
                    <View style={{ width: '50%', position: 'absolute', marginLeft: '50%', padding: 3 }}>
                      <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>Password</Text>
                      <View>
                        <View style={{ width: '100%', height: 40 }}>
                          <TextInput ref={(input) => { this.password = input; }} onSubmitEditing={() => { this.skey.focus(); }} secureTextEntry={this.state.hidden} style={{ fontFamily: 'kanitLight',borderRadius:20 , color: '#fff', height: 40, borderColor: 'gray', borderWidth: 1, marginTop: 5, paddingLeft: 20 }} onChangeText={(password) => this.setState({ password })} value={(this.state.password)} />
                        </View>
                        <View style={{ width: '100%', height: 40, alignItems: 'flex-end', position: 'absolute' }}>

                          <TouchableOpacity style={{ width: 60, height: 40, marginTop: 5, justifyContent: 'center', alignItems: 'center' }} onPress={this.onInputLabelPressed}>
                            <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>
                              {this.state.hidden ? 'Show' : 'Hide'}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </View>
                  <View style={{ width: '100%', padding: 3,marginBottom:15 }}>
                    <Text style={{ fontFamily: 'kanitLight', color: '#fff' }}>SecretKey</Text>
                    <View style={{ alignItems: 'center' }}>
                      <SmoothPinCodeInput password mask="﹡"
                        ref={(input) => { this.skey = input; }}
                        cellStyle={{ borderRadius:20 , borderColor: 'gray', borderWidth: 1, marginTop: 5 }}
                        cellSize={40}
                        codeLength={6}
                        value={this.state.secretkey}
                        onTextChange={secretkey => this.setState({ secretkey })
                        }
                      /></View>
                  </View>
                  <View style={{ width: '100%',height:40,marginBottom:15,fontFamily: 'kanitLight', backgroundColor: 'rgba(0,0,0,0)', borderColor: '#fff', marginTop: 5 , borderRadius:20 , borderWidth: 1, maxWidth: 600}}>
                    <Button title="Register" style={{ width: '100%',height:'100%',backgroundColor: 'rgba(0,0,0,0)', borderColor: 'rgba(0,0,0,0)', height: 40 }} onPress={() => this.CreateCustomer()} ><Text style={{color:'#fff'}}>SIGN UP</Text></Button>
                  </View>
                </View>
                
              </View>
            </View>
          </TouchableWithoutFeedback>
          
          </LinearGradient>
          <Alert
            alertAnimatedIcon
            alertAutoHide
            alertTitle={this.state.alerttitle}
            alertMessage={this.state.alertmessage}
            alertBGColor={this.state.alertcolor}
            style={{fontFamily: 'kanitLight' ,}}
          />

        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render



  CreateCustomer() {

    console.log(this.state.checkemail)
    //console.log('createcustomerwork');
    if (this.state.firstname != '' && this.state.lastname != '' && this.state.tel != '' && this.state.date != '' && this.state.email != '' && this.state.username != '' && this.state.password != '' && this.state.secretkey != '') {
      if (this.state.checkemail == 'true' && this.state.checkemail != undefined) {
        axios({
          method: 'post',
          url: URLSERVICE + 'customer/all',
          TIMEOUT: 10000,
          data: { firstname: this.state.firstname, lastname: this.state.lastname, tel: this.state.tel, birthday: this.state.date, username: this.state.username, password: md5(this.state.password), email: this.state.email, usertype: 1, secretkey: md5(this.state.secretkey) }
        }).then(res => {
          //console.log(res);
          console.log("Register Complete")
          const { navigate } = this.props.navigation;
          navigate('Home');
        }
        ).catch(err => {
          this.setState({
            alerttitle: 'Register',
            alertmessage: 'Register Not Successful Please TryAgain',
            alertcolor: 'red'
          })
          Alert.showAlert();
        })
      } else {
        this.setState({
          alerttitle: 'Email',
          alertmessage: 'Email Invaild Format',
          alertcolor: 'orange'
        })
        Alert.showAlert();
      }
    } else {
      //alert
      this.setState({
        alerttitle: 'Warning',
        alertmessage: 'Input can not blank!',
        alertcolor: 'orange'
      })
      Alert.showAlert();
    }

  }
}


const mapStateToProps = state => {
  return {
  }
}
const mapDispatchToProps = {
  startloading,
  stoploading,
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});