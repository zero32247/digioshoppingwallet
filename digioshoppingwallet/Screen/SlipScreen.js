import React from 'react';
import { View, Text, StyleSheet, Button, Image, PermissionsAndroid, TouchableOpacity, ImageBackground } from 'react-native'
import ViewShot from "react-native-view-shot";
import * as Permissions from 'expo-permissions';
import * as MediaLibrary from 'expo-media-library';
import { storeUserData, storeMoneyData } from '../actions/user';
import { connect } from 'react-redux';
import axios from 'axios';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import Alert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';

async function saveImage(uri) {
  await MediaLibrary.saveToLibraryAsync(uri).then(function (result) {
    console.log('save succeeded');
  }).catch(function (error) {
    console.log('save failed ' + error);
  });
  console.log('FINISHED');

}

async function requestSaveSlipImage(uri) {
  const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
  console.log(permission.status);

  if (permission.status !== 'granted') {
    const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (newPermission.status === 'granted') {
      console.log("granted");
      saveImage(uri);

    }
  } else {
    saveImage(uri);
  }
}

state = {
  customers: [],
  showprogress: false,
  username: '',
  merchantid: '',
  merchantname: '',
  total: '',
  customerid: '',
  money: '',
  sliptype: '',
  firstname: '',
  lastname: '',
  alerttitle: '',
  alertmessage: '',
  alertcolor: '',
  orderid: '',
  fontloading: false
}

class SlipScreen extends React.Component {

  constructor() {
    super();
    this.state = {};
  }


  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })
    const { startloading, stoploading } = this.props;
    stoploading();
  }

  componentWillMount() {
    const { storeUserData, dispatch, user } = this.props;
    console.log("Recive" + this.props.navigation.state.params);
    const merchantname = this.props.navigation.state.params.merchantname;
    const total = this.props.navigation.state.params.total;
    const sliptype = this.props.navigation.state.params.SlipType;
    const firstname = this.props.navigation.state.params.firstname;
    const lastname = this.props.navigation.state.params.lastname;
    const orderid = this.props.navigation.state.params.orderid;
    this.setState({
      money: user.money,
      merchantname: merchantname,
      total: total,
      username: user.username,
      sliptype: sliptype,
      firstname: user.firstname,
      lastname: user.lastname,
      orderid: orderid
    })
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    var hour = new Date().getHours();
    var minute = new Date().getMinutes();
    var second = new Date().getSeconds();

    this.setState({

      maxday:
        year + "-" + month + "-" + date + "  " + hour + ":" + minute + ":" + second,
    });
    setTimeout(() => {
      this.refs.viewShot.capture().then(uri => {
        console.log('RESULTurl:', uri);
        this.setState({
          uri: uri
        })
        requestSaveSlipImage(uri);
        this.setState({
          alerttitle: 'Saved Slip',
          alertmessage: 'Slip saved to your device.',
          alertcolor: 'green'
        })
        Alert.showAlert();
      });
    }, 500);
  }

  UpdateWallet() {
    const { storeUserData, dispatch, user, storeMoneyData, startloading, stoploading } = this.props;
    startloading();
    axios.get(URLSERVICE + `wallet/` + user.walletid)
      .then(res => {
        console.log(res.data)
        const users = res.data;
        storeMoneyData(res.data.money);
        console.log("123456");
        //const { navigate } = this.props.navigation;
        //navigate('Profile');
        stoploading();
        this.GOTOWALLET();
      })
  }

  GOTOWALLET() {
    console.log('------------------------------------');
    console.log("NAVIWORK");
    console.log('------------------------------------');
    const { navigate } = this.props.navigation;
    navigate('Profile');
  }
  render() {
    const { startloading, stoploading } = this.props;
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View style={{ height: '100%', backgroundColor: '#262833' }}>
          <View style={{ height: '100%', width: '100%' }}>
            <ViewShot ref="viewShot" options={{ format: "png", quality: 1 }} >
              <View style={{ height: '16%', width: '100%', backgroundColor: '#262833' }}>
                
            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%',height:'10%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text></View>
            </View>
              </View>

              <View style={{ height: '84%', width: '100%', paddingLeft: 30, paddingRight: 30, paddingBottom: 30, backgroundColor: '#262833' }}>
                <View style={{ width: '100%', position: 'absolute', marginLeft: 30, height: 100, marginTop: -50, zIndex: 1, alignItems: 'center' }}>
                  <View style={{ width: 100, height: 100, borderRadius: 500, backgroundColor: '#fff' }}>
                    <Image
                      style={{ width: 90, height: 90, margin: 5 }}
                      source={require('../assets/correct2.png')}
                    />
                  </View>

                </View>

                <View style={{ height: '95%', width: '100%', maxHeight: 700, alignItems: 'center' }}>
                  <View style={{ backgroundColor: '#fff', maxWidth: 400, height: '95%', width: '100%', borderRadius: 10 }}>
                    {/*Thankyou */}
                    <View style={{ backgroundColor: '#dcdcdc', maxWidth: 400, paddingTop: 45, height: '25%', width: '100%', borderTopRightRadius: 10, borderTopLeftRadius: 10 }}>
                      <Text style={{ fontFamily: 'kanitLight', backgroundColor: '#dcdcdc', textAlign: 'center', fontSize: 30 }}>Thank You</Text>
                      <Text style={{ fontFamily: 'kanitLight', fontSize: 10, textAlign: 'center' }}> Date: {this.state.maxday}</Text>
                      <Text style={{ fontFamily: 'kanitLight', fontSize: 10, textAlign: 'center' }}> {this.state.orderid}</Text>

                    </View>

                    {/*Detail */}
                    <View style={{ height: '30%', maxWidth: 400, paddingTop: 25, paddingBottom: 20, paddingLeft: 20, paddingRight: 20, width: '100%', backgroundColor: '#fff' }}>
                      <View style={{ width: '100%' }}>
                        <View style={{ width: '100%', height: '50%' }}>
                          <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>By</Text>
                          <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>{this.state.firstname}  {this.state.lastname}</Text>
                        </View>
                        <View style={{ width: '100%' }}>
                          <Text style={{ fontFamily: 'kanitLight'}}>{this.state.merchantwalletid}</Text>
                        </View>
                      </View>
                      <View style={{ width: '100%' }}>
                        <View style={{ width: '100%', height: '50%' }}>
                          <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>To</Text>
                          <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>{this.state.merchantname}</Text>
                        </View>
                        <View style={{ width: '100%' }}>
                          <Text>{this.state.merchantwalletid}</Text>
                        </View>
                      </View>
                      <View style={{ width: '100%', height: 40,marginTop:15 }}>
                    <View style={{ width: '50%', position: 'absolute', bottom: 0, height: 40 }}>
                      <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase', position: 'absolute', bottom: 0}}>Total (THB) : </Text>
                    </View>
                    <View style={{ width: '50%', position: 'absolute', right: 0, bottom: 0, height: 40 }}>
                      <Text style={{ fontFamily: 'kanitLight', textAlign: 'right', fontSize: 30 }}>{this.state.total} ฿</Text>
                    </View>
                  </View>
                      {/* 2 dot */}
                      <View style={{ position: 'absolute', height: 30, width: 15, backgroundColor: '#262833', borderTopRightRadius: 15, borderBottomRightRadius: 15, marginTop: -15 }}><Text style={{ fontFamily: 'kanitLight', color: 'rgba(0,0,0,0)' }}>.</Text></View>
                      <View style={{ position: 'absolute', right: 0, height: 30, width: 15, backgroundColor: '#262833', borderTopLeftRadius: 15, borderBottomLeftRadius: 15, marginTop: -15 }}><Text style={{ fontFamily: 'kanitLight', color: 'rgba(0,0,0,0)' }}>.</Text></View>

                      {/* order Detail */}
                    </View>
                  </View>
                </View>
              </View>
            </ViewShot>
            <Alert
              alertAnimatedIcon
              alertAutoHide
              alertTitle={this.state.alerttitle}
              alertMessage={this.state.alertmessage}
              alertBGColor={this.state.alertcolor}
            />
          </View>


          <View style={{ width: '100%', position: 'absolute', bottom: 0 }}>
            <TouchableOpacity style={{ width: '100%', height: 50 }} onPress={() => {
              this.UpdateWallet();
            }}>
              <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 15, color: '#fff' }}>Back to wallet</Text>
            </TouchableOpacity>

          </View>
          {this.state.uri
            &&
            <Image style={{ marginLeft: 100, width: 250, height: 250 }} source={{ uri: this.state.uri }}></Image>
          }


        </View>
      );
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render

}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  storeMoneyData,
  startloading,
  stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(SlipScreen);