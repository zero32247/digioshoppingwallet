import React from 'react';
import axios from 'axios';
import { View, Text, StyleSheet, TouchableOpacity, Vibration, Image, Alert, Modal, TouchableHighlight } from 'react-native'
import Constants from 'expo-constants';
import { storeUserData, storeBioAuthData } from '../actions/user';
import { Dimensions } from "react-native";
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import TopAlert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { startloading, stoploading } from '../actions/loading';
import { connect } from 'react-redux';
import { copilot, walkthroughable, CopilotStep } from '@okgrow/react-native-copilot';
import PropTypes from 'prop-types';
import { ThemeConsumer } from 'react-native-elements';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import * as LocalAuthentication from 'expo-local-authentication';
import FastImage from 'react-native-fast-image';
async function alertIfRemoteNotificationsDisabledAsync() {
  const { status } = await Permissions.getAsync(Permissions.BarCodeScanner);
  if (status !== 'granted') {
    alert('Hey! You might want to enable notifications for my app, they are good.');
  }
}
async function checkMultiPermissions() {
  const { status, expires, permissions } = await Permissions.getAsync(
    permissions.CAMERA,
  );
  if (status !== 'granted') {
    alert('Hey! You heve not enabled selected permissions');
  }
}

const CopilotView = walkthroughable(View);

class ProfileScreen extends React.Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };


  state = {
    username: '',
    firstname: '',
    lastname: '',
    money: '',
    usertype: '',
    topup: false,
    customer: false,
    customerid: '',
    fontloading: false,
    tutorial: '',
    compatible: false,
    fingerprints: false,
    authenticated: false,
    modalVisible: false,
    failedCount: 0,
    fingerScanText: '',
    fingerbtnText: '',
    timeout: true,
    
  }
  toggleStatus() {
    this.setState({
      status: !this.state.status
    });
    console.log('toggle button handler: ' + this.state.status);
  }
  async componentDidMount() {

    let { status } = Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermissionsGranted: (status === 'granted') });
    const { storeUserData, dispatch, user } = this.props;
    //console.log("SECRETKEY" + user.secretkey)
    this.setState({
      username: user.username,
      firstname: user.firstname,
      lastname: user.lastname,
      money: user.money,
      usertype: user.usertype,
      walletid: user.walletid,
      customerid: user.customerid,
      tutorial: user.logincount,
    })

    if (user.logincount.includes('Profile') == false) {
      this.props.copilotEvents.on('stepChange', this.handleStepChange);
      this.props.start();
    }
    //console.log("usertype::" + user.usertype);
    if (user.usertype == 1) {
      this.setState({ customer: true })
    } else if (user.usertype == 3) {

      this.setState({ topup: true })
    }

    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })

    console.log("PROFILE LOGIN COUNT:   " + user.logincount)

    const { startloading, stoploading } = this.props;
    stoploading();

    this.props.copilotEvents.on('stop', () => {
      this.UpdateTutorial();
    });
    this.checkDeviceForHardware();
    this.checkForFingerprints();

    if (user.logincount.includes('Profile') == false) {
      if (this.state.compatible == true && this.state.fingerprints == true) {
        this.BioAuthConfirmAlert();
      }
    }

  }

  BioAuthConfirmAlert() {
    Alert.alert(
      'Authication BioMectric',
      'Login With FingerPrint',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: 'Confrim', onPress: () => this.setModalVisible(true) },
      ],
      { cancelable: false }
    );
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  scanFingerPrint = async () => {
    try {
      let results = await LocalAuthentication.authenticateAsync();
      if (results.success) {
        this.setState({
          modalVisible: false,
          authenticated: true,
          failedCount: 0,
        });
        this.SaveBioAuth()
      } else {
        console.log('Before Failed Count:   ' + this.state.failedCount)
        this.setState({
          failedCount: this.state.failedCount + 1,
          fingerbtnText: 'Scan Again',
          fingerScanText: 'Scan FingerPrint Failed Please TryAgain'
        });
        console.log('After Failed Count:   ' + this.state.failedCount)
      }
    } catch (e) {
      console.log(e);
    }
  };

  SaveBioAuth() {
    const { storeBioAuthData } = this.props;
    storeBioAuthData(true);
  }

  checkDeviceForHardware = async () => {
    let compatible = await LocalAuthentication.hasHardwareAsync();
    this.setState({ compatible });
  };

  checkForFingerprints = async () => {
    let fingerprints = await LocalAuthentication.isEnrolledAsync();
    this.setState({ fingerprints });
  };

  UpdateTutorial() {
    console.log('------------------------------------');
    console.log("Username:   " + this.state.username);
    console.log('------------------------------------');
    axios({
      method: 'post',
      url: URLSERVICE + 'user/tooltip/' + this.state.username,
      timeout: 30000,
      data: "Profile"
    }).then(res => {
      console.log("Update Tutorail Complete");
      if (this.state.compatible == true && this.state.fingerprints == true) {
        this.BioAuthConfirmAlert();
      }
    }
    )
  }

  handleStepChange = (step) => {
    console.log(`Current step is: ${step.name}`);
  }

  componentWillMount() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    const screenHeight = Math.round(Dimensions.get('window').height);
    this.setState({ screenWidth: screenWidth, screenHeight: screenHeight })
  }
  componentWillReceiveProps(nextProps) {
    const { storeUserData, dispatch, user } = nextProps;
    console.log('------------------------------------');
    console.log("USERMONEYSSSSSS" + user.money);
    console.log('------------------------------------');
    this.setState({
      money: user.money,
    })
  }
  _handleNotification = notification => {
    Vibration.vibrate()
    this.setState({
      alerttitle: 'TopUp',
      alertmessage: 'Topup Successful',
      alertcolor: 'green'
    })
    TopAlert.showAlert();
    this.setState({ notification: notification });
  };
  sendPushNotification = async () => {
    console.log('------------------------------------');
    console.log("EXPOTOKEN:    " + this.state.expoPushToken);
    console.log('------------------------------------');
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),
    });
    const data = response._bodyInit;
    console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
  };

  CheckTimeout(navitype) {
    const { user, startloading, stoploading } = this.props;
    startloading();
    axios.get(URLSERVICE + 'user/timeout/' + user.username)
      .then(res => {
        console.log("TIMEOUT RES:  " + res.data)
        if (res.data == 'timeout') {
          stoploading();
          console.log("TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT TIMEOUT");
          Alert.alert(
            'Timeout',
            'No Activity Toolong Please Login Again',
            [
              { text: 'Confrim', onPress: () => this.Home() },
            ],
            { cancelable: false }
          );
        } else {
          stoploading();
          if (navitype == 'SCAN') {
            this.NaviScanPayment();
          } else if (navitype == 'Transaction') {
            this.TransactionHistory();
          } else if (navitype == 'Topup') {
            this.TopUpScan();
          } else if (navitype == 'WalletQR') {
            this.TopUpWallet();
          }
        }
      }).catch(err => {
        stoploading();
        console.log("TIMEOUT ERROR:   " + err)
      })
  }


  NaviScanPayment() {
    const { navigate } = this.props.navigation;
    navigate('ScanPayment');
  }


  TopUpWallet() {
    const { user } = this.props;
    const { navigate } = this.props.navigation;
    navigate('WalletQR', { expoPushToken: user.expoPushToken });
  }


  TopUpScan() {
    const { navigate } = this.props.navigation;
    navigate('ScanTopup');
  }


  Home() {
    const { navigate } = this.props.navigation;
    navigate('Home');
  }

  TransactionHistory() {
    const { navigate } = this.props.navigation;
    navigate('TransactionHistory');
  }

  Setting() {
    const { navigate } = this.props.navigation;
    navigate('Setting');
  }

  sendPushNotification = async () => {
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),

    });
    const data = response._bodyInit;
    console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
  };
  static navigationOptions = {
    title: 'Profile',
  };
  render() {
    const { startloading, stoploading } = this.props;
    const { navigate } = this.props.navigation;
    //console.log(this.props)
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%', height: '10%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text></View>
                <TouchableOpacity style={{ position: 'absolute', right:5, width: 30, height: 30, justifyContent: "center", alignItems: "center" }} onPress={() => this.Setting()} disabled={this.state.showprogress}>
                  <View style={{ width: '100%', height: '100%', justifyContent: "center", alignItems: "center" }}>
                    <Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/wrench1.png')}
                  /></View>
              </TouchableOpacity>
            </View>
            <View style={{ width: '100%', height: '20%', justifyContent: "center" }}>
              <View>
                <CopilotStep text="This your Balance" order={2} name="hello2">
                  <CopilotView>
                    <View>
                      {this.state.topup &&
                        <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>TOPUP ONLY</Text>
                      }
                      {this.state.customer &&
                        <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>฿ {(this.state.money).toFixed(2)}</Text>
                      }
                    </View>
                  </CopilotView>
                </CopilotStep>
              </View>
              <View style={{ height: 20 }}><Text style={{
                fontFamily: 'kanitLight', fontSize: 13

                , color: '#777', textAlign: 'center'
              }}>BALANCE</Text></View>
            </View>

            <View style={{ width: '100%', height: '70%', paddingBottom: 10, backgroundColor: '#fff', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
            <CopilotStep text="This your profile" order={1} name="hello1">
                <CopilotView style={{ width:'100%',height:'100%' }}>
                  <View style={{ textAlign: 'center', paddingTop: 20 }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'center', color: '#222', textTransform: 'uppercase' }}>{this.state.firstname} {this.state.lastname}</Text>
              </View>
              <View style={{ textAlign: 'center', paddingBottom: 10, Top: 0 }}>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textAlign: 'center', color: '#222' }}>CUSTOMER NO. {this.state.customerid}</Text>
              </View>
              </CopilotView>
              </CopilotStep>

            </View>

            <View style={{ position: 'absolute', bottom: 0, backgroundColor: '#fff', width: '100%', height: 80 }}>

              <TouchableOpacity style={{ position: 'absolute', left: '10%', width: '23%', height: '100%', justifyContent: "center", alignItems: "center" }} onPress={() => this.CheckTimeout('Transaction')} disabled={this.state.showprogress}>
                <CopilotStep text="This your TransactionHistory" order={4} name="hello4">
                  <CopilotView style={{ width: '100%', height: '100%', justifyContent: "center", alignItems: "center" }}><Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/bank2.png')}
                  /><Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>History</Text></CopilotView>
                </CopilotStep>
              </TouchableOpacity>

              {this.state.customer &&
                <TouchableOpacity style={{ position: 'absolute', right: '33%', width: '33%', height: '100%', justifyContent: "center", alignItems: "center" }} onPress={() => this.CheckTimeout('SCAN')} disabled={this.state.showprogress}>
                  <CopilotStep text="Scan QRCode Here" order={3} name="hello3">
                    <CopilotView style={{ width: 70, height: 70, justifyContent: "center", alignItems: "center", borderColor: '#aaa', borderRadius: 35, paddingTop: 5, borderWidth: 2 }}><Image
                      style={{ width: 30, height: 30 }}
                      source={require('../assets/qr-scan2.png')}
                    /><Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>SCAN</Text></CopilotView>
                  </CopilotStep>
                </TouchableOpacity>
              }
              {this.state.topup &&
                <TouchableOpacity style={{ position: 'absolute', right: '10%', width: '24%', height: '100%', justifyContent: "center", alignItems: "center" }} onPress={() => this.CheckTimeout('Topup')} disabled={this.state.showprogress}>
                  <Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/scan2.png')}
                  /><Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>TopUp</Text>
                </TouchableOpacity>
              }
              {this.state.customer &&
                <TouchableOpacity style={{ position: 'absolute', right: '10%', width: '24%', height: '100%', justifyContent: "center", alignItems: "center" }} onPress={() => this.CheckTimeout('WalletQR')} disabled={this.state.showprogress}>
                  <CopilotStep text="This your Topup QrCode" order={5} name="hello5">
                    <CopilotView style={{ width: '100%', height: '100%', justifyContent: "center", alignItems: "center" }}><Image
                      style={{ width: 20, height: 20 }}
                      source={require('../assets/scan2.png')}
                    /><Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>My QRcode</Text></CopilotView>
                  </CopilotStep>
                </TouchableOpacity>
              }
            </View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={this.state.modalVisible}
              onShow={this.scanFingerPrint}>
              <View style={styles.modal}>
                <View style={styles.innerContainer}>
                  <Text>Sign in with fingerprint</Text>
                  <Image
                    style={{ width: 128, height: 128 }}
                    source={require('../assets/fingerprint.png')}
                  />
                  {this.state.failedCount > 0 && (
                    <Text style={{ color: 'red', fontSize: 14 }}>
                      {this.state.fingerScanText}
                    </Text>

                  )}
                  <TouchableHighlight
                    onPress={async () => {
                      // LocalAuthentication.cancelAuthenticate();
                      // this.setModalVisible(!this.state.modalVisible);
                      this.scanFingerPrint()
                    }}>
                    <Text style={{ color: 'red', fontSize: 16 }}>{this.state.fingerbtnText}</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </Modal>
            <TopAlert
              alertAnimatedIcon
              alertAutoHide
              alertTitle={this.state.alerttitle}
              alertMessage={this.state.alertmessage}
              alertBGColor={this.state.alertcolor}
            />
          </LinearGradient>
        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render

}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}
const mapDispatchToProps = {
  storeUserData,
  storeBioAuthData,
  startloading,
  stoploading,
}
export default copilot({ animated: true, verticalOffset: 0 })(connect(mapStateToProps, mapDispatchToProps)(ProfileScreen));
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    paddingTop: Constants.statusBarHeight,
    padding: 8,
  },
  modal: {
    flex: 1,
    marginTop: '90%',
    backgroundColor: '#E5E5E5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    marginTop: '30%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    alignSelf: 'center',
    fontSize: 22,
    paddingTop: 20,
  },
});