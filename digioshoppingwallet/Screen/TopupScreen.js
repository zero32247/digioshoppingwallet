import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, TextInput, ImageBackground, TouchableWithoutFeedback, TouchableOpacity, Image, Keyboard } from 'react-native'
import Button from 'apsl-react-native-button'
import { storeUserData } from '../actions/user';
import { connect } from 'react-redux';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import TopAlert from '@logisticinfotech/react-native-animated-alert';
import { startloading, stoploading } from '../actions/loading';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';


class TopupScreen extends React.Component {
  static navigationOptions = {
    title: 'Topup',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0,
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  state = {
    customerid: '',
    walletid: '',
    amount: '',
    secretkey: '',
    password: '',
    expoPushToken: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    firstname: '',
    lastname: '',
    key: '',
    fontloading: false
  }



  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })
    const username = this.props.navigation.state.params.username;
    const walletid = this.props.navigation.state.params.walletid;
    const customerid = this.props.navigation.state.params.customerid;
    const firstname = this.props.navigation.state.params.firstname;
    const lastname = this.props.navigation.state.params.lastname;
    //const expoPushToken = this.props.navigation.state.params.expoPushToken;
    const { user } = this.props;
    console.log("Walletid" + walletid)
    this.setState({
      secretkey: user.secretkey,
      username: username,
      customerid: customerid,
      walletid: walletid,
      firstname: firstname,
      lastname: lastname
    })
    const { startloading, stoploading } = this.props;
    stoploading();
  }

  /*componentWillReceiveProps(){
    const expoPushToken = this.props.navigation.state.params.expoPushToken;
    this.setState({
      expoPushToken : expoPushToken,
    })
  }*/



  CheckSercretKey() {
    const { navigate } = this.props.navigation;
    console.log('TopUp Amount:   '+this.state.amount)
    if (this.state.key > 0) {
      navigate('SecretKeyCheck', {
        username: this.state.username,
        customerid: this.state.customerid,
        walletid: this.state.walletid,
        firstname: this.state.firstname,
        lastname: this.state.lastname,
        amount: this.state.key,
        type: 'Profile',
      })
    } else if (this.state.key <= 0) {
      this.setState({
        alerttitle: 'Top',
        alertmessage: 'Money Topup must be more than 0',
        alertcolor: 'red'
      })
      TopAlert.showAlert();
    }

  }
  setkey(num) {
    this.setState({ key: this.state.key + num });
    console.log("cus:::" + this.state.customerid);
  }
  render() {
    const { startloading, stoploading } = this.props;
    //console.log(this.props)
    const { password } = this.state;
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View>
          <TopAlert style={{ fontFamily: 'kanitLight', }} alertAnimatedIcon alertAutoHide alertTitle={this.state.alerttitle} alertMessage={this.state.alertmessage} alertBGColor={this.state.alertcolor} />
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%', height: '7%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text></View>
            </View>
            <View style={{ width: '100%', height: '15%', justifyContent: "center" }}>
              <View>
                <View>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>TOPUP</Text>
                </View>
              </View>
              <View style={{ height: 20 }}><Text style={{
                fontFamily: 'kanitLight', fontSize: 18

                , color: '#ccc', textAlign: 'center'
              }}>{this.state.firstname}  {this.state.lastname}</Text></View>
            </View>
            <View style={{ width: '100%', height: '85%', borderTopLeftRadius: 20, borderTopRightRadius: 20 }}>
              <View style={{ alignItems: 'center' }}>
                <View style={{ height: "80%" }}>
                  <View style={{ marginTop: 5, padding: 5 }}><Text style={{ fontFamily: 'kanitLight', textAlign: 'center', color: '#fff' }}>Amount</Text></View>
                  <View style={{ padding: 5, width: '100%', alignItems: 'center' }}>
                    <TextInput style={{ color: '#fff', paddingLeft: 10, width: 250, height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 100 }} keyboardType='numeric' onFocus={Keyboard.dismiss} onChangeText={(amount) => this.setState({ amount })} value={(this.state.key)} />
                  </View>
                  <View style={{ alignItems: 'center', width: '100%', maxWidth: 400, padding: 5 }} >
                    <View style={{ alignItems: 'center', width: 240 }}>

                      <View style={{ left: 0, width: 80, position: 'absolute' }}>
                        {/* 1 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('1')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>1</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 4 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('4')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>4</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 7 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('7')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>7</Text>
                          </View>
                        </TouchableOpacity>
                        {/* . */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('.')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>.</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={{ alignItems: 'center', width: 80, position: 'absolute' }}>
                        {/* 2 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('2')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>2</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 5 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('5')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>5</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 8 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('8')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>8</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 0 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('0')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>0</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={{ right: 0, width: 80, position: 'absolute' }}>
                        {/* 3 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('3')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>3</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 6 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('6')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>6</Text>
                          </View>
                        </TouchableOpacity>
                        {/* 9 */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('9')}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25, color: '#fff' }}>9</Text>
                          </View>
                        </TouchableOpacity>
                        {/* < */}
                        <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setState({ key: this.state.key.substring(0, this.state.key.length - 1) })}>
                          <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                            <Image
                              style={{ width: 30, height: 30, margin: 15 }}
                              source={require('../assets/ui.png')}
                            />
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, backgroundColor: 'rgba(255,255,255,0.2)' }} >
              <TouchableWithoutFeedback style={{ alignItems: 'center', width: '100%', height: "100%" }} onPress={() => this.CheckSercretKey()} disabled={this.state.showprogress}>

                <View style={{ width: '100%', height: 50, justifyContent: 'center', }}>
                  <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>Confirm</Text>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </LinearGradient>
        </View>
      )
      //   <View style={{ height: '100%' }}>

      //     <LinearGradient
      //       colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
      //       style={{ alignItems: 'center', height: "100%" }}>

      //       <View style={{ position: 'absolute', width: '100%', height: "100%", marginTop: 60, paddingBottom: 15, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5 }}>
      //         <View style={{  height: "80%", margin: 20 }}>
      //           <View style={{ padding: 5, width: '100%' }}><Text style={{ fontFamily: 'kanitLight', fontSize: 30, textAlign: 'center',color:'#fff' }}>TOPUP</Text></View>
      //           <Text style={{ fontFamily: 'kanitLight', marginTop: 5, textAlign: 'center', width: '100%',color:'#fff' }}>Customer Name : {this.state.firstname}  {this.state.lastname}</Text>
      //           <View style={{ marginTop: 5, padding: 10 }}><Text style={{ fontFamily: 'kanitLight', textAlign: 'center',color:'#fff' }}>Amount</Text></View>
      //           <View style={{ padding: 5, width: '100%', alignItems: 'center' }}><TextInput style={{ paddingLeft: 10, width: 250, height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 10 }} keyboardType='numeric' onFocus={Keyboard.dismiss} onChangeText={(amount) => this.setState({ amount })} value={(this.state.key)} />
      //           </View>
      //           <View style={{ alignItems: 'center', width: '100%', maxWidth: 400, padding: 5 }} >
      //             <View style={{ alignItems: 'center', width: 240 }}>

      //               <View style={{ left: 0, width: 80, position: 'absolute' }}>
      //                 {/* 1 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('1')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>1</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 4 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('4')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>4</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 7 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('7')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>7</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* . */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('.')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>.</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //               </View>
      //               <View style={{ alignItems: 'center', width: 80, position: 'absolute' }}>
      //                 {/* 2 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('2')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>2</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 5 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('5')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>5</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 8 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('8')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>8</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 0 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('0')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>0</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //               </View>
      //               <View style={{ right: 0, width: 80, position: 'absolute' }}>
      //                 {/* 3 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('3')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>3</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 6 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('6')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>6</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* 9 */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('9')}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25,color:'#fff' }}>9</Text>
      //                   </View>
      //                 </TouchableOpacity>
      //                 {/* < */}
      //                 <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setState({ key: this.state.key.substring(0, this.state.key.length - 1) })}>
      //                   <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
      //                     <Image
      //                       style={{ width: 30, height: 30, margin: 15 }}
      //                       source={require('../assets/ui.png')}
      //                     />
      //                   </View>
      //                 </TouchableOpacity>
      //               </View>
      //             </View>
      //           </View>
      //         </View>
      //         <View style={{ width: '100%', height: 50, position: 'absolute', bottom: 0, marginBottom: 60, backgroundColor: '#fff' }} >
      //           <TouchableWithoutFeedback style={{ alignItems: 'center', width: '100%', height: "100%" }} onPress={() => this.CheckSercretKey()} disabled={this.state.showprogress}>

      //             <View style={{ width: '100%', height: 50, justifyContent: 'center', }}>
      //               <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#020202' }}>Confirm</Text>
      //             </View>
      //           </TouchableWithoutFeedback>
      //         </View>
      //       </View>
      //     </LinearGradient>
      //   </View>

      // )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render

}


const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  startloading,
  stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(TopupScreen);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});