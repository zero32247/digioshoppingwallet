import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, ImageBackground, TouchableWithoutFeedback, TouchableOpacity, Image, Keyboard } from 'react-native'
import Button from 'apsl-react-native-button'
import { SkypeIndicator, WaveIndicator, BarIndicator } from 'react-native-indicators';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import { storeUserData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import Alert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';


class CodeTopupScreen extends React.Component {
    static navigationOptions = {
        title: 'CodeTopup',
        headerTransparent: {
            position: 'absolute',
            backgroundColor: 'transparent',
            zIndex: 100,
            top: 0,
            left: 0,
            right: 0
        },
        headerTitleStyle: {
            color: 'white'
        },
        headerTintColor: 'white',
    };
    state = {
        username: '',
        customerid: '',
        walletid: '',
        firstname: '',
        lastname: '',
        key: '',
        fontloading: false
    }

    async componentDidMount() {
        await Font.loadAsync({
            kanitLight: require('../assets/fonts/Kanit-Light.ttf')
        });
        this.setState({ fontloading: true })
        const { startloading, stoploading } = this.props;
        stoploading();
    }
    getCustomerData() {
        const { startloading, stoploading } = this.props;
        if (this.state.temporary != '') {
            startloading();
            axios.get(URLSERVICE + `customer/id/` + this.state.key, { timeout: TIMEOUT })
                .then(res => {
                    console.log(res.data);
                    if (res.data != '') {
                        stoploading();
                        const { navigate } = this.props.navigation;
                        navigate('Topup', {
                            username: res.data.username,
                            customerid: this.state.key,
                            walletid: res.data.walletid,
                            firstname: res.data.firstname,
                            lastname: res.data.lastname
                        });
                    }
                }).catch(err => {
                    stoploading();
                    console.log("CATCH")
                    this.setState({
                        alerttitle: 'Code Topup',
                        alertmessage: 'Cannot Get CustomerData',
                        alertcolor: 'red',
                        temporary: ''
                    })
                    Alert.showAlert();
                })
        } else {
            stoploading();
            this.setState({
                alerttitle: 'Code Topup',
                alertmessage: 'Incorrect Customerid',
                alertcolor: 'red',
                temporary: ''
            })
            Alert.showAlert();
        }

    }

    setkey(num) {
        const secret = this.state.key;
        if (secret.length < 6) {
            this.setState({ key: secret + num });
        }
    }




    render() {
        const { startloading, stoploading } = this.props;
        const { customerid } = this.state;
        //console.log(this.props)
        const { fontloading } = this.state;
        if (fontloading) {
            return (
                <View style={{ height: '100%' }}>

                    <LinearGradient
                        colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                        style={{ alignItems: 'center', height: "100%" }}>


                        <View style={{ position: 'absolute', width: '100%', height: "100%", marginTop: 80, paddingBottom: 80, shadowColor: "#000", shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 5 }}>

                            <View style={{ width: '100%', marginTop: 10, marginBottom: 10 }}>
                                <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 ,color: '#fff'}}>Enter Code</Text>
                                <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 10 ,color: '#fff'}}>Enter Payment Code here</Text>
                            </View>
                            <View style={{ alignItems: 'center' }}>
                                <SmoothPinCodeInput
                                    ref={(input) => { this.skey = input; }}
                                    cellStyle={{ borderRadius:100 , borderColor: 'gray', borderWidth: 1, marginTop: 5 }}
                                    cellStyleFocused={{
                                        borderColor: 'black',
                                    }}
                                    textStyle={{
                                        fontSize: 24,
                                      color: '#fff'
                                    }}
                                    codeLength={6}
                                    value={this.state.key}
                                    onTextChange={temporary => this.setState({ temporary })}
                                    onPress={() => Keyboard.dismiss()}
                                    onFocus={() => Keyboard.dismiss()}
                                />
                            </View>

                            <View style={{ alignItems: 'center', width: '100%' }}>
                                <View style={{ alignItems: 'center', width: '100%', maxWidth: 400, padding: 20 }} >
                                    <View style={{ alignItems: 'center', width: 240 }}>

                                        <View style={{ left: 0, width: 80, position: 'absolute' }}>
                                            {/* 1 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('1')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff', fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>1</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 4 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('4')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>4</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 7 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('7')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>7</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/*  */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }}>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ alignItems: 'center', width: 80, position: 'absolute' }}>
                                            {/* 2 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('2')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>2</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 5 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('5')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>5</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 8 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('8')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>8</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 0 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('0')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>0</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ right: 0, width: 80, position: 'absolute' }}>
                                            {/* 3 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('3')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>3</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 6 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('6')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>6</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* 9 */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setkey('9')}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Text style={{color:'#fff',  fontFamily: 'kanitLight', textAlign: 'center', fontSize: 25 }}>9</Text>
                                                </View>
                                            </TouchableOpacity>
                                            {/* < */}
                                            <TouchableOpacity style={{ width: '100%', height: 80, justifyContent: "center" }} onPress={() => this.setState({ key: this.state.key.substring(0, this.state.key.length - 1) })}>
                                                <View style={{ backgroundColor: "rgba(255,255,255,0.2)", width: 60, height: 60, borderRadius: 35, margin: 10, justifyContent: "center" }}>
                                                    <Image
                                                        style={{ width: 30, height: 30, margin: 15,color:'#fff' }}
                                                        source={require('../assets/ui.png')}
                                                    />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                            {/* BOTTOM */}
        <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
          <View style={{ bottom: 0, position: 'absolute', width: '100%', height: 70 }}>
          <LinearGradient
                colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style={{ alignItems: 'center', width: '100%', height: '100%', bottom: 0, justifyContent: 'center' }}>
              
                <View style={{width: 250 , height:40 , justifyContent: 'center' }}>
                <LinearGradient
                colors={['#44b6ec', '#3494c1', '#2081af']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                style={{ width: '100%', height: '100%',borderRadius:20,paddingTop:5 }}>
                  <TouchableWithoutFeedback onPress={() => this.getCustomerData()} disabled={this.state.showprogress}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>TOPUP</Text>
                  </TouchableWithoutFeedback>
                  </LinearGradient>
                </View>
            
            </LinearGradient>
          </View>
        </View>
                            
                        </View>
                        <Alert
                            alertAnimatedIcon
                            alertAutoHide
                            alertTitle={this.state.alerttitle}
                            alertMessage={this.state.alertmessage}
                            alertBGColor={this.state.alertcolor}
                        />
                    </LinearGradient>
                </View>
            )
        } else {
            return (
                <View>
                    {startloading()}
                </View>
            )
        }
    }//end render

}

const mapStateToProps = state => {
    return {
        data: state.app.data,
        user: state.user,
    }
}

const mapDispatchToProps = {
    storeUserData,
    startloading,
    stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(CodeTopupScreen);



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
    },
});