import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import { BackHandler } from 'react-native';
import { View, Text, StyleSheet, TextInput, TouchableWithoutFeedback, Image } from 'react-native';
import Button from 'apsl-react-native-button'
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';
import { Dimensions } from "react-native";
import Alert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';
import { copilot, walkthroughable, CopilotStep } from '@okgrow/react-native-copilot';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { storeUserData, storeTutorialData } from '../actions/user';


async function alertIfRemoteNotificationsDisabledAsync() {
  const { status } = await Permissions.getAsync(Permissions.BarCodeScanner);
  if (status !== 'granted') {
    alert('Hey! You might want to enable notifications for my app, they are good.');
  }
}

async function checkMultiPermissions() {
  const { status, expires, permissions } = await Permissions.getAsync(
    permissions.CAMERA,
  );
  if (status !== 'granted') {
    alert('Hey! You heve not enabled selected permissions');
  }
}

const CopilotView = walkthroughable(View);
class ScanPaymentScreen extends React.Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    stop: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
      off: PropTypes.func.isRequired,
    }).isRequired,
  };


  static navigationOptions = {
    title: 'ScanPayment',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'white'
    },
    headerTintColor: 'white',
  };


  UNSAFE_componentWillMount() {

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    console.log("UNMOUNT WORKS");
    this.props.copilotEvents.off('stop');
  }

  handleBackButtonClick() {
    this.props.navigation.goBack(null);
    this.setState(this.state)
    return true;
  }
  constructor() {
    super();
    this.state = { screenWidth: "", screenHeight: "" }
  }
  state = {
    username: '',
    merchantid: '',
    merchantname: '',
    merchantwalletid: '',
    orderid: '',
    total: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    secondStepActive: true,
    tutorialfinish: false,
  }

  async componentDidMount() {
    const { user } = this.props;
    this.setState({
      username: user.username,
    })
    if (user.logincount.includes('ScanPaymentScreen') == false) {
      this.props.copilotEvents.on('stepChange', this.handleStepChange);
      this.props.start();
    } else {
      this.setState({ tutorialfinish: true })
    }
    let { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasPermissionsGranted: (status === 'granted') });

    this.props.copilotEvents.on('stop', () => {
      this.UpdateTutorial();
      this.setState({ tutorialfinish: true })
    });
  }

  UpdateTutorial() {
    const { storeTutorialData } = this.props;
    axios({
      method: 'post',
      url: URLSERVICE + 'user/tooltip/' + this.state.username,
      timeout: 30000,
      data: "ScanPaymentScreen"
    }).then(res => {
      console.log("Update Tutorail Complete");
      storeTutorialData(this.state.username);
    }
    )
  }


  handleStepChange = (step) => {
    console.log(`Current step is: ${step.name}`);
  }




  Getqrcodedata(data) {
    const input = data.data;
    const [merchantid, merchantname, merchantwalletid, orderid, total] = input.split(',');
    const screenWidth = Math.round(Dimensions.get('window').width);
    const screenHeight = Math.round(Dimensions.get('window').height);
    this.setState({
      merchantid: merchantid,
      merchantname: merchantname,
      merchantwalletid: merchantwalletid,
      orderid: orderid,
      total: total,
      screenWidth: screenWidth,
      screenHeight: screenHeight
    })
    this.onScannedComplete(merchantid, merchantname, merchantwalletid, orderid, total);
  }



  onScannedComplete(merchantid, merchantname, merchantwalletid, orderid, total) {

    console.log("merchantwalletid" + this.state.merchantwalletid);
    console.log(merchantwalletid != undefined);
    if ((merchantid != "undefined") == false || (merchantwalletid != undefined) == false || (orderid != undefined) == false || (total != undefined) == false) {
      this.setState({
        alerttitle: 'QR Code',
        alertmessage: 'Scan QR Code Failed Please Scan Again or Payment By Code',
        alertcolor: 'orange'
      })
      Alert.showAlert();
    } else if ((this.state.merchantid != undefined) == true && (this.state.merchantwalletid != undefined) == true && (this.state.orderid != undefined) == true && (this.state.total != undefined) == true) {
      console.log("STATE CHECK" + (this.state.merchantwalletid) + "     " + (this.state.total));
      console.log("STATE CHECK" + (this.state.merchantwalletid != "") + "     " + (this.state.total != ""));
      if ((this.state.merchantwalletid != "") == true && (this.state.total != "") == true && this.state.tutorialfinish == true) {
        this.gotoPaymentScreen();
      } else {
        console.log("FAILED")
      }

    }

  }
  componentWillMount() {
    const screenWidth = Math.round(Dimensions.get('window').width);
    const screenHeight = Math.round(Dimensions.get('window').height);
    this.setState({ screenWidth: screenWidth, screenHeight: screenHeight })
  }
  gotoPaymentScreen() {

    const { navigate } = this.props.navigation;
    navigate('Payment', {
      merchantid: this.state.merchantid,
      merchantname: this.state.merchantname,
      merchantwalletid: this.state.merchantwalletid,
      orderid: this.state.orderid,
      total: this.state.total,
    });
  }

  Codepayment() {
    console.log('------------------------------------');
    console.log("CODEPAYMENT");
    console.log('------------------------------------');
    const { navigate } = this.props.navigation;
    navigate('CodePayment');
  }

  TopUpWallet() {
    const { user } = this.props;
    const { navigate } = this.props.navigation;
    navigate('WalletQR', { expoPushToken: user.expoPushToken });
  }


  render() {
    //console.log(this.props)
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    return (
      <View style={{ height: '100%', width: '100%' }} >

        <View style={{ height: '100%', width: '100%' }}>
          <BarCodeScanner
            onBarCodeScanned={data => this.Getqrcodedata(data)}
            barCodeTypes={[
              BarCodeScanner.Constants.BarCodeType.qr,
              BarCodeScanner.Constants.BarCodeType.pdf417,
            ]}
            style={[StyleSheet.absoluteFill, styles.cameraContainer]}
            type={this.state.type}
          >
            <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}>
            <CopilotStep text="Scan QrCode Here" order={1} name="hello1">
                     <CopilotView>
              <Image
                style={{ width: this.state.screenWidth * 0.6, height: this.state.screenWidth * 0.6 }}
                source={require('../assets/borderqr.png')}
              />
              </CopilotView>
              </CopilotStep>
              </View>
          </BarCodeScanner>

        </View>
        {/* TOP */}
        <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
          <View style={{ position: 'absolute', width: '100%', height: 120 }}>
            <LinearGradient
              colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
              style={{ width: '100%', height: 120, bottom: 0, paddingTop: 80 }}>
              <View style={{ width: '50%', bottom: 0, height: 40, position: 'absolute',paddingRight:10 }}>
                <TouchableWithoutFeedback disabled={this.state.showprogress}
                  style={{  width: '100%', height: 40 }}>
                  <View style={{ width: '100%', height: 40,alignItems: 'flex-end'}}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff', height: 40, borderBottomWidth: 5, borderColor: '#399cca' }}>SCAN</Text>
                  </View>
                </TouchableWithoutFeedback></View>
              <View style={{ width: '50%', bottom: 0, height: 40, position: 'absolute', marginLeft: '50%',paddingLeft:10 }}>
                <TouchableWithoutFeedback onPress={() => this.TopUpWallet()} disabled={this.state.showprogress}
                  style={{  width: '100%', height: 40 }}>
                    
                  <View style={{ width: 120, height: 40 }}><CopilotStep text="Your QRcode click here" order={2} name="hello2">
                     <CopilotView>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff', height: 40 }}>MY QRCODE</Text>
                    </CopilotView>
                  </CopilotStep>
                  </View>
                </TouchableWithoutFeedback></View>
              

            </LinearGradient>
          </View>
        </View>
        {/* BOTTOM */}
        <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
          <View style={{ bottom: 0, position: 'absolute', width: '100%', height: 120 }}>
            <LinearGradient
              colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
              style={{ alignItems: 'center', width: '100%', height: '100%', bottom: 0, justifyContent: 'center' }}>
 <CopilotStep text="If cannot Scan Qrcode use Codepayment" order={3} name="hello3">
                     <CopilotView>
              <View style={{ width: 250, height: 40, justifyContent: 'center' }}>
                <LinearGradient
                  colors={['#44b6ec', '#3494c1', '#2081af']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
                  style={{ width: '100%', height: '100%', borderRadius: 20, paddingTop: 5 }}>
                  <TouchableWithoutFeedback onPress={() => this.Codepayment()} disabled={this.state.showprogress}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 20, color: '#fff' }}>CODE PAYMENT</Text>
                  </TouchableWithoutFeedback>
                </LinearGradient>
              </View>
              </CopilotView>
              </CopilotStep>

            </LinearGradient>
          </View>
        </View>

      </View>
    )
    // return (
    //   <View style={{ height: '100%', width: '100%' }} >

    //     <View style={{ height: '100%', width: '100%' }}>
    //       <BarCodeScanner
    //         //onBarCodeScanned={data => alert(JSON.stringify(data.data))}
    //         onBarCodeScanned={data => this.Getqrcodedata(data)}
    //         barCodeTypes={[
    //           BarCodeScanner.Constants.BarCodeType.qr,
    //           BarCodeScanner.Constants.BarCodeType.pdf417,
    //         ]}
    //         style={[StyleSheet.absoluteFill, styles.cameraContainer]}
    //         type={this.state.type}
    //       >

    //         <View style={{ alignItems: 'center', justifyContent: 'center', height: '100%', width: '100%' }}>
    //         <CopilotStep text="Scan QrCode Here" order={1} name="hello1">
    //                 <CopilotView><Image
    //             style={{ width: this.state.screenWidth * 0.6, height: this.state.screenWidth * 0.6 }}
    //             source={require('../assets/borderqr.png')}
    //           /></CopilotView>
    //           </CopilotStep></View>
    //       </BarCodeScanner>
    //       <Alert
    //         alertAnimatedIcon
    //         alertAutoHide
    //         alertTitle={this.state.alerttitle}
    //         alertMessage={this.state.alertmessage}
    //         alertBGColor={this.state.alertcolor}
    //       />
    //     </View>
    //     <View style={{ width: '100%', height: '100%', position: 'absolute' }}>
    //       <View style={{ bottom: 0, position: 'absolute', width: '100%' }}>
    //       <CopilotStep text="If cannot Scan Qrcode use Codepayment" order={2} name="hello2">
    //                 <CopilotView>
    //         <TouchableWithoutFeedback onPress={() => this.Codepayment()} disabled={this.state.showprogress}>
    //           <LinearGradient
    //             colors={['#192f6a', '#3b5998', '#4c669f']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
    //             style={{ alignItems: 'center', height: 50, bottom: 0, justifyContent: 'center' }}>
    //             <View style={{ width: '100%', justifyContent: 'center', }}>
    //               <Text style={{ textAlign: 'center', fontSize: 20, color: '#fff' }}>Code Payment</Text>
    //             </View>
    //           </LinearGradient>
    //         </TouchableWithoutFeedback>
    //         </CopilotView>
    //           </CopilotStep>
    //       </View>
    //     </View>

    //   </View>
    // )
  }

}



const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}
const mapDispatchToProps = {
  storeUserData,
  storeTutorialData
}

export default copilot({ animated: true })(connect(mapStateToProps, mapDispatchToProps)(ScanPaymentScreen));
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});