import React from 'react';
import { View, Text, StyleSheet, TextInput, ImageBackground, Vibration,TouchableOpacity,Image } from 'react-native'
import { QRCode } from 'react-native-custom-qr-codes-expo';
import Button from 'apsl-react-native-button'
import { storeUserData, storeMoneyData,storeTutorialData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import axios from 'axios';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import Alert from '@logisticinfotech/react-native-animated-alert';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import { LinearGradient } from 'expo-linear-gradient';
import SvgUri from 'react-native-svg-uri';
import * as Font from 'expo-font';
import { copilot, walkthroughable, CopilotStep } from '@okgrow/react-native-copilot';
import PropTypes from 'prop-types';

const CopilotView = walkthroughable(View);
class WalletQRScreen extends React.Component { 
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };

state = {
    username:'',
    customerid: '',
    walletid: '',
    firstname: '',
    lastname: '',
    expoPushToken: '',
    notification: {},
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    fontloading: false,
    secondStepActive: true,
  }
  
  static navigationOptions = {
    title: 'WalletQR',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  





  async componentDidMount() {
    const { storeUserData, dispatch, user } = this.props;
    console.log("WALLETQR:  "+user.logincount)
    if (user.logincount.includes('WalletQRScreen') == false) {
      this.props.copilotEvents.on('stepChange', this.handleStepChange);
      this.props.start();
    }
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });

    this.setState({ fontloading: true })
    const expoPushToken = this.props.navigation.state.params.expoPushToken;

    this.setState({
      username:user.username,
      customerid: user.customerid,
      walletid: user.walletid,
      firstname: user.firstname,
      lastname: user.lastname,
      expoPushToken: expoPushToken
    })
    this._notificationSubscription = Notifications.addListener(
      this._handleNotification
    );
    const { startloading, stoploading } = this.props;

    stoploading();

    this.props.copilotEvents.on('stop', () => {
      this.UpdateTutorial();
    });
  }

  UpdateTutorial() {
    const { storeTutorialData } = this.props;

    axios({
      method: 'post',
      url: URLSERVICE + 'user/tooltip/' + this.state.username,
      timeout: 30000,
      data: "WalletQRScreen"
    }).then(res => {
      console.log("Update Tutorail Complete");
      storeTutorialData(this.state.username);
    }
    )
  }



  handleStepChange = (step) => {
    console.log(`Current step is: ${step.name}`);
  }

  _handleNotification = notification => {
    Vibration.vibrate()
    //this.AlertNoti();
    this.setState({
      alerttitle: 'TopUp',
      alertmessage: 'Topup Successful',
      alertcolor: 'green'
    })
    Alert.showAlert();
    this.UpdateWallet();
    this.setState({ notification: notification });
  };

  sendPushNotification = async () => {
    const response = await fetch('https://exp.host/--/api/v2/push/send', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Accept-encoding': 'gzip, deflate',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(message),

    });
    const data = response._bodyInit;
    console.log(`Status & Response ID-> ${JSON.stringify(data)}`);
  };


  UpdateWallet() {
    const { user, storeMoneyData, startloading, stoploading } = this.props;
    startloading();
    console.log('------------------------------------');
    console.log("WALLETID" + user.walletid);
    console.log('------------------------------------');

    axios.get(URLSERVICE + `wallet/` + user.walletid)
      .then(res => {
        console.log(res.data)
        const users = res.data;
        storeMoneyData(res.data.money);
        stoploading();
        this.GOTOWALLET();
      }).catch(err => {
        console.log("Update Wallet Error:   " + err)
        this.setState({
          alerttitle: 'Network',
          alertmessage: 'NetWork Error',
          alertcolor: 'red'
        })
      })
  }

  GOTOWALLET() {
    console.log('------------------------------------');
    console.log("NAVIWORK");
    console.log('------------------------------------');
    const { navigate } = this.props.navigation;
    navigate('Profile');
  }

  Setting() {
    const { navigate } = this.props.navigation;
    navigate('Setting');
  }
  render() {
    const { startloading, stoploading } = this.props;
    const { navigate } = this.props.navigation;
    const { user } = this.props; const { fontloading } = this.state;
    //console.log(this.props)
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    if (fontloading) {
      return (
        <View>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%',height:'10%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text>
                <TouchableOpacity style={{ position: 'absolute', right:5, width: 30, height: 30, justifyContent: "center", alignItems: "center" }} onPress={() => this.Setting()} disabled={this.state.showprogress}>
                  <View style={{ width: '100%', height: '100%', justifyContent: "center", alignItems: "center" }}><Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/wrench1.png')}
                  /></View>
              </TouchableOpacity></View>
            </View>
                <View style={{ width: '100%',height:'15%', justifyContent: "center" }}>
                  <View>
                        <View>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>QRCODE</Text>
                        </View>
                  </View>
                  <View style={{ height: 20 }}><Text style={{
                    fontFamily: 'kanitLight', fontSize: 13

                    , color: '#777', textAlign: 'center'
                  }}>WALLET</Text></View>
                </View>
              
            <View style={{ width: '100%',height:'75%', paddingBottom: 10 ,backgroundColor:'#fff',borderTopLeftRadius:20,borderTopRightRadius:20 }}>
              <View style={{ textAlign: 'center', paddingTop: 20 }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'center', color: '#222', textTransform: 'uppercase' }}>{this.state.firstname} {this.state.lastname}</Text>
              </View>
              <CopilotStep text="This your customerid" order={2} name="hello2">
                <CopilotView style={{ textAlign: 'center', paddingBottom: 20, Top: 0 }}>
                  <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textAlign: 'center', color: '#222' }}>CUSTOMER NO. {this.state.customerid}</Text>
                </CopilotView>
              </CopilotStep>
              <View style={{alignItems: 'center'}}>
              <CopilotStep text="Topup please Scan QRcode" order={1} name="hello1" >
                     <CopilotView style={{ backgroundColor: '#fff', shadowColor: "#000", alignItems: 'center', shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 20,}}>
                       <QRCode style={{backgroundColor: '#fff'}} content={user.username + "," + user.customerid + "," + user.walletid} />
                     </CopilotView>
                   </CopilotStep>
                   </View>
            </View>
          </LinearGradient>
        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
//     const { startloading, stoploading } = this.props;
//     const { user } = this.props; const { fontloading } = this.state;
//     if (fontloading) {
//       return (
//         <View style={{ alignItems: 'center', height: "100%", width: '100%' }}>

//           <LinearGradient
//             colors={['#192f6a', '#3b5998', '#4c669f']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
//             style={{ alignItems: 'center', height: "100%", width: '100%' }}>
//             <View style={{ position: 'absolute', alignItems: 'center', height: '55%', backgroundColor: '#fff', width: '100%', bottom: 0 }}>
//               <View style={{ position: 'absolute', backgroundColor: '#fff', alignItems: 'center', padding: 30, width: '100%', height: "100%" }}>

//                 <View style={{ height: '20%', width: '100%', alignItems: 'center' }}>
//                   <CopilotStep text="Topup please Scan QRcode" order={1} name="hello1" >
//                     <CopilotView style={{
//                       backgroundColor: '#fff', position: 'absolute',
//                       bottom: 0, shadowColor: "#000", alignItems: 'center', shadowOffset: { width: 0, height: 2, }, shadowOpacity: 0.25, shadowRadius: 3.84, elevation: 20,
//                     }}>

//                       <QRCode style={{backgroundColor: '#fff'}} content={user.username + "," + user.customerid + "," + user.walletid} />

//                     </CopilotView>
//                   </CopilotStep>
//                 </View>
//                 <View style={{ marginTop: 20 }}>
//                   <Text>name </Text>
//                 </View>
//                 <View style={{ height: '40%', padding: 5 }}><Text style={{ fontFamily: 'kanitLight', fontSize: 20 }}>{this.state.firstname}  {this.state.lastname}</Text></View>
//                 <CopilotStep text="Press here to back home" order={2} name="hello2" >
//                   <CopilotView style={{ height: '20%', marginLeft: '5%', marginRight: '5%', marginTop: 10, marginBottom: 10 }}>
// <View>
//                     <Button style={{ width: '100%', height: '100%', backgroundColor: '#6c9fea', borderColor: '#6c9fea' }} title="BackToWallet" onPress={() => this.UpdateWallet()} >BackToWallet</Button>
//                     </View>
//                   </CopilotView>
//                 </CopilotStep>
//               </View>


//             </View>
//           </LinearGradient>
//         </View>
//       )
//     } else {
//       return (
//         <View>
//           {startloading()}
//         </View>
//       )
//     }
  }//end render

}


const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  startloading,
  storeMoneyData,
  storeTutorialData,
  stoploading
}

export default copilot({verticalOffset: 0,justifyContent: 'center',alignItems: 'Left',paddingHorizontal:0})(connect(mapStateToProps, mapDispatchToProps)(WalletQRScreen));



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});