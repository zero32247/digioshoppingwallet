import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, TextInput, Button, FlatList, ImageBackground, TouchableOpacity,Image, TouchableHighlight, TouchableWithoutFeedback } from 'react-native'
import { Permissions } from 'expo-permissions';
import { storeUserData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import Alert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import * as Font from 'expo-font';

class TransactionHistoryScreen extends React.Component {
  static navigationOptions = {
    title: 'Transaction',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  state = {
    transaction: [],
    customerid: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    fontloading: false
  }

  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })

    const { user } = this.props;
    console.log('------------------------------------');
    console.log("Customerid" + user.customerid);
    console.log('------------------------------------');
    this.setState({
      customerid: user.customerid
    })
    this.getTransactionHistory(user.customerid);
  }

  setTime(time) {
    //const millisec = Math.round((new Date()).getTime());
    const millsec = new Date(parseInt(time));
    //console.log("TIME:   "+millisec+"      "+parseInt(time))
    //console.log("Date:    "+millsec+"       "+millsec.getDate() + "/" + millsec.getMonth() + "/" + millsec.getFullYear() + " " + millsec.getHours() + ":" + millsec.getMinutes());
    return millsec.getDate() + "/" + (millsec.getMonth() + 1) + "/" + millsec.getFullYear() + " " + millsec.getHours() + ":" + millsec.getMinutes();
  }


  Setting() {
    const { navigate } = this.props.navigation;
    navigate('Setting');
  }
  render() {
    const { startloading, stoploading } = this.props;
    const { navigate } = this.props.navigation;
    const { user } = this.props; const { fontloading } = this.state;
    //console.log(this.props)
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }
    if (fontloading) {
      return (
        <View>
          <LinearGradient
            colors={['#3b3f5c', '#262833', '#282a3b']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 1 }}
            style={{ alignItems: 'center', height: "100%" }}>

            <View style={{ textAlign: 'center', marginTop: 25, paddingBottom: 20, width: '100%',height:'7%' }}>
              <View style={{ width: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'right', color: '#fff' }}>DIGIO</Text></View>
              <View style={{ width: '50%', position: 'absolute', marginLeft: '50%' }}>
                <Text style={{ fontFamily: 'kanitLight', fontSize: 15, textAlign: 'left', color: '#888' }}>WALLET</Text>
                <TouchableOpacity style={{ position: 'absolute', right:5, width: 30, height: 30, justifyContent: "center", alignItems: "center" }} onPress={() => this.Setting()} disabled={this.state.showprogress}>
                  <View style={{ width: '100%', height: '100%', justifyContent: "center", alignItems: "center" }}><Image
                    style={{ width: 20, height: 20 }}
                    source={require('../assets/wrench1.png')}
                  /></View>
              </TouchableOpacity></View>
            </View>
                <View style={{ width: '100%',height:'18%', justifyContent: "center" }}>
                  <View>
                        <View>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 35, color: '#fff', textAlign: 'center' }}>TRANSACTION</Text>
                        </View>
                  </View>
                  <View style={{ height: 20 }}><Text style={{
                    fontFamily: 'kanitLight', fontSize: 13

                    , color: '#777', textAlign: 'center'
                  }}>WALLET</Text></View>
                </View>
              
            <View style={{ width: '100%',height:'75%', paddingBottom: 10 ,backgroundColor:'#fff',borderTopLeftRadius:20,borderTopRightRadius:20 }}>
              <View style={{alignItems: 'center'}}>
              <FlatList style={{ width: '100%',marginTop:15,marginBottom:15,borderTopLeftRadius:20,borderTopRightRadius:20 }}
                data={this.state.transaction}
                renderItem={({ item }) =>
                  <TouchableWithoutFeedback onPress={() => this.TransactionDetail(item.transactionid, item.transactiontype,item.transactiontime,item.merchantname,item.amount)}>
                    <View style={{ borderBottomColor: '#cdcdcd', borderBottomWidth: 1, width: '100%' }}>
                      {item.transactiontype == "Pay" &&
                        <View style={{ marginRight: 5 }}>
                        <View style={{ backgroundColor: '#ca0000', width: 7, position: 'absolute', height: 56,borderRadius:4,margin:2 }}></View>
                          <View style={{ backgroundColor: '#fff', width: '49%', height: 60, marginLeft: 12, paddingLeft: 10 }}>
                            <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20, textTransform: 'uppercase' }}>{item.transactiontype}</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textTransform: 'uppercase' }} numberOfLines={1}>AT : {item.merchantname}</Text>
                            </View>
                          <View style={{ backgroundColor: '#fff', width: '55%', right: 0, position: 'absolute', height: 60, paddingRight: 5 }}>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: '#ca0000' }}>- {item.amount} ฿</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 8, textAlign: 'right' }}>{this.setTime(item.transactiontime)}</Text>
                          </View>
                        </View>
                      }
                      {item.transactiontype == "TopUp" &&
                        <View style={{marginRight: 5}}>

<View style={{ backgroundColor: '#12ba01', width: 7, position: 'absolute', height: 56,borderRadius:4,margin:2 }}></View>
                          <View style={{ backgroundColor: '#fff', width: '49%', height: 60, marginLeft: 12, paddingLeft: 10 }}>
                            <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20, textTransform: 'uppercase' }}>{item.transactiontype}</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textTransform: 'uppercase' }} numberOfLines={1}>AT : {item.merchantname}</Text>
                            </View>
                          <View style={{ backgroundColor: '#fff', width: '50%', right: 0, position: 'absolute', height: 60, paddingRight: 5 }}>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: '#12ba01' }}>+ {item.amount}  ฿</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 8, textAlign: 'right' }}>{this.setTime(item.transactiontime)}</Text>
                          </View>
                        </View>
                      }
                      {item.transactiontype == "withdraw" &&
                        <View style={{marginRight: 5}}>
    <View style={{ backgroundColor: '#0169ba', width: 7, position: 'absolute', height: 56,borderRadius:4,margin:2 }}></View>
                          <View style={{ backgroundColor: '#fff', width: '50%', height: 60, marginLeft: 12, paddingLeft: 10 }}>
                            <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20, textTransform: 'uppercase' }}>{item.transactiontype}</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 12, textTransform: 'uppercase' }} numberOfLines={1}>AT : {item.merchantname}</Text>
                            </View>
                          <View style={{ backgroundColor: '#fff', width: '50%', right: 0, position: 'absolute', height: 60, paddingRight: 5 }}>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: '#0169ba' }}>- {item.amount} ฿</Text>
                            <Text style={{ fontFamily: 'kanitLight', fontSize: 8, textAlign: 'right' }}>{this.setTime(item.transactiontime)} </Text>
                          </View>
                        </View>
                      }
                    </View>
                  </TouchableWithoutFeedback>
                }
              />
                   </View>
            </View>
          </LinearGradient>
        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
    // const { startloading, stoploading } = this.props;
    // //console.log(this.props)
    // const { fontloading } = this.state;
    // if (fontloading) {
    //   return (
    //     <View>
    //       <LinearGradient
    //         colors={['#192f6a', '#3b5998', '#4c669f']} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }}
    //         style={{ alignItems: 'center', height: "100%" }}>
    //         <View style={{ height: '10%' }}></View>
    //         <View style={{ height: '90%', width: '100%', paddingTop: 20 }}>
    //           <FlatList style={{ backgroundColor: 'rgba(255,255,255,0.7)', width: '100%' }}
    //             data={this.state.transaction}
    //             renderItem={({ item }) =>
    //               <TouchableWithoutFeedback onPress={() => this.TransactionDetail(item.transactionid, item.transactiontype,item.transactiontime,item.merchantname,item.amount)}>
    //                 <View style={{ borderBottomColor: '#cdcdcd', borderBottomWidth: 1, width: '100%' }}>
    //                   {item.transactiontype == "Pay" &&
    //                     <View style={{ backgroundColor: '#ff0000', marginLeft: 10, marginRight: 10, marginTop: 2, marginBottom: 2, borderRadius: 5 }}>
    //                       <View style={{ backgroundColor: '#fff', width: '49%', height: 70, marginLeft: 7, paddingLeft: 10 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20 }}>{item.transactiontype}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 15 }}>At : {item.merchantname}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>{this.setTime(item.transactiontime)}</Text>
    //                       </View>
    //                       <View style={{ backgroundColor: '#fff', width: '55%', right: 0, position: 'absolute', height: 70, paddingRight: 10, borderBottomRightRadius: 5, borderTopRightRadius: 5 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: 'red' }}>- {item.amount}</Text>
    //                       </View>
    //                     </View>
    //                   }
    //                   {item.transactiontype == "TopUp" &&
    //                     <View style={{ backgroundColor: '#00ff00', marginLeft: 10, marginRight: 10, marginTop: 2, marginBottom: 2, borderRadius: 5 }}>

    //                       <View style={{ backgroundColor: '#fff', width: '49%', height: 70, marginLeft: 7, paddingLeft: 10 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20 }}>{item.transactiontype}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 15 }}>At : {item.merchantname}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>{this.setTime(item.transactiontime)}</Text>
    //                       </View>
    //                       <View style={{ backgroundColor: '#fff', width: '50%', right: 0, position: 'absolute', height: 70, paddingRight: 10, borderBottomRightRadius: 5, borderTopRightRadius: 5 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: '#00ff00' }}>+ {item.amount}</Text>
    //                       </View>
    //                     </View>
    //                   }
    //                   {item.transactiontype == "withdraw" &&
    //                     <View style={{ backgroundColor: '#0000ff', marginLeft: 10, marginRight: 10, marginTop: 2, marginBottom: 2, borderRadius: 5 }}>

    //                       <View style={{ backgroundColor: '#fff', width: '50%', height: 70, marginLeft: 7, paddingLeft: 10 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', paddingTop: 1, fontSize: 20 }}>{item.transactiontype}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 15 }}>At : {item.merchantname}</Text>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 8 }}>{this.setTime(item.transactiontime)}</Text>
    //                       </View>
    //                       <View style={{ backgroundColor: '#fff', width: '50%', right: 0, position: 'absolute', height: 70, paddingRight: 10, borderBottomRightRadius: 5, borderTopRightRadius: 5 }}>
    //                         <Text style={{ fontFamily: 'kanitLight', fontSize: 20, textAlign: 'right', color: '#0000ff' }}>- {item.amount}</Text>
    //                       </View>
    //                     </View>
    //                   }
    //                 </View>
    //               </TouchableWithoutFeedback>
    //             }
    //           />
    //         </View>

    //         <Alert
    //           alertAnimatedIcon
    //           alertAutoHide
    //           alertTitle={this.state.alerttitle}
    //           alertMessage={this.state.alertmessage}
    //           alertBGColor={this.state.alertcolor}
    //         />
    //       </LinearGradient>
    //     </View>
    //   )
    // } else {
    //   return (
    //     <View>
    //       {startloading()}
    //     </View>
    //   )
    // }
  }//end render



  getTransactionHistory(customerid) {
    const { startloading, stoploading } = this.props;
    startloading();
    console.log("Customerid" + customerid)
    axios.get(URLSERVICE + `transaction/customer/` + customerid, { timeout: TIMEOUT })
      .then(res => {
        console.log(res.data)
        //debugger
        this.setState({ transaction: res.data })
        stoploading();
      }).catch(err => {
        //debugger
        stoploading();
        this.setState({
          alerttitle: 'Transaction',
          alertmessage: 'Cannot Get TransactionHistory',
          alertcolor: 'red'
        })
        Alert.showAlert();
      })
  }

  TransactionDetail(transactionid, transactiontype,transactiontime,merchantname,amount) {
    console.log('------------------------------------');
    console.log("Transactionid:    " + transactionid + "       " + transactiontype+"      "+transactiontime);
    console.log('------------------------------------');
    if (transactiontype == 'Pay') {
      const { navigate } = this.props.navigation;
      navigate('TransactionDetail', {
        transactionid: transactionid,
        transactiontime:transactiontime,
        merchantname:merchantname,
        amount:amount
      })
    }
  }

}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  startloading,
  stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionHistoryScreen);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});