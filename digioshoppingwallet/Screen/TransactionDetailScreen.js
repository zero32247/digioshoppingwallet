import React from 'react';
import md5 from 'md5';
import axios from 'axios';
import { View, Text, StyleSheet, Image, FlatList } from 'react-native'
import { Permissions } from 'expo-permissions';
import { storeUserData } from '../actions/user';
import { connect } from 'react-redux';
import { startloading, stoploading } from '../actions/loading';
import { URLSERVICE, TIMEOUT } from '../config/configvalue';
import Alert from '@logisticinfotech/react-native-animated-alert';
import { LinearGradient } from 'expo-linear-gradient';
import SvgUri from 'react-native-svg-uri';
import * as Font from 'expo-font';


class TransactionDetailScreen extends React.Component {
  static navigationOptions = {
    title: 'TransactionDetail',
    headerTransparent: {
      position: 'absolute',
      backgroundColor: 'transparent',
      zIndex: 100,
      top: 0,
      left: 0,
      right: 0
    },
    headerTitleStyle: {
      color: 'transparent'
    },
    headerTintColor: 'white',
  };

  state = {
    orderdetail: [],
    transactionid: '',
    transactiontime:'',
    merchantname:'',
    amount:'',
    customerid: '',
    alerttitle: '',
    alertmessage: '',
    alertcolor: '',
    fontloading: false
  }

  async componentDidMount() {
    await Font.loadAsync({
      kanitLight: require('../assets/fonts/Kanit-Light.ttf')
    });
    this.setState({ fontloading: true })

    const { user } = this.props;
    console.log('------------------------------------');
    console.log("Customerid" + user.customerid);
    console.log('------------------------------------');
    const transactionid = this.props.navigation.state.params.transactionid;
    const transactiontime = this.props.navigation.state.params.transactiontime;
    const merchantname = this.props.navigation.state.params.merchantname;
    const amount = this.props.navigation.state.params.amount;
    this.setState({
      transaction: transactionid,
      transactiontime:transactiontime,
      merchantname:merchantname,
      amount:amount
    })
    this.getOrderDetail(transactionid);
    const { startloading, stoploading } = this.props;
    stoploading();
  }

  setTime(time) {
    const millsec = new Date(parseInt(time));
    return millsec.getDate() + "/" + millsec.getMonth() + "/" + millsec.getFullYear() + " " + millsec.getHours() + ":" + millsec.getMinutes();
  }


  render() {
    const { startloading, stoploading } = this.props;
    //console.log(this.props)
    const { fontloading } = this.state;
    if (fontloading) {
      return (
        <View style={{ backgroundColor: '#262833' }}>


          <View style={{ height: '100%', width: '100%' }}>

            <View style={{ height: '20%', width: '100%' }}>
            </View>

            <View style={{ height: '80%', width: '100%', paddingLeft: 30, paddingRight: 30, paddingBottom: 30 }}>
              <View style={{ width: '100%', position: 'absolute', marginLeft: 30, height: 100, marginTop: -50, zIndex: 1, alignItems: 'center' }}>
                <View style={{ width: 100, height: 100, borderRadius: 500, backgroundColor: '#fff' }}>
                  <Image
                    style={{ width: 90, height: 90, margin: 5 }}
                    source={require('../assets/correct2.png')}
                  />
                </View>

              </View>

              <View style={{ height: '100%', width: '100%', maxHeight: 700, alignItems: 'center' }}>

                {/*Thankyou */}
                <View style={{ backgroundColor: '#dcdcdc', maxWidth: 400, paddingTop: 45, height: '25%', width: '100%', borderTopRightRadius: 10, borderTopLeftRadius: 10}}>
                  <Text style={{ fontFamily: 'kanitLight', backgroundColor: '#dcdcdc', textAlign: 'center', fontSize: 30 }}>THANK YOU</Text>
                  <Text style={{ fontFamily: 'kanitLight', backgroundColor: '#dcdcdc', textAlign: 'center', fontSize: 15 }}>PAY</Text>
                </View>

                {/*Detail */}
                <View style={{ height: '30%', maxWidth: 400, paddingTop: 25, paddingBottom: 20, paddingLeft: 20, paddingRight: 20, width: '100%', backgroundColor: '#fff' }}>
                  <View style={{ width: '100%' }}>
                    <View style={{ width: '100%' }}>
                      <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>Date : {this.setTime(this.state.transactiontime)}</Text>
                    </View>
                    <View style={{ width: '100%',marginTop:10}}>
                      <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase'}}>At : {this.state.merchantname}</Text>
                    </View>
                  </View>

                  <View style={{ width: '100%', height: 40,marginTop:15 }}>
                    <View style={{ width: '50%', position: 'absolute', bottom: 0, height: 40 }}>
                      <Text style={{ fontFamily: 'kanitLight', textTransform: 'uppercase', position: 'absolute', bottom: 0}}>Total (THB) : </Text>
                    </View>
                    <View style={{ width: '50%', position: 'absolute', right: 0, bottom: 0, height: 40 }}>
                      <Text style={{ fontFamily: 'kanitLight', textAlign: 'right', fontSize: 30 }}>{this.state.amount} ฿</Text>
                    </View>
                  </View>
                  {/* 2 dot */}
                  <View style={{ position: 'absolute', height: 30, width: 15, backgroundColor: '#262833', borderTopRightRadius: 15, borderBottomRightRadius: 15, marginTop: -15 }}><Text style={{ fontFamily: 'kanitLight', color: 'rgba(0,0,0,0)' }}>.</Text></View>
                  <View style={{ position: 'absolute', right: 0, height: 30, width: 15, backgroundColor: '#262833', borderTopLeftRadius: 15, borderBottomLeftRadius: 15, marginTop: -15 }}><Text style={{ fontFamily: 'kanitLight', color: 'rgba(0,0,0,0)' }}>.</Text></View>

                  {/* order Detail */}
                </View>
                <View style={{ width: '100%', height: '45%', maxWidth: 400, padding: 20, backgroundColor: '#fff', borderBottomRightRadius: 10, borderBottomLeftRadius: 10 }}>
                  <View style={{ width: '100%', backgroundColor: '#ccc',borderRadius:10 }}>
                    <Text style={{ fontFamily: 'kanitLight', textAlign: 'center', fontSize: 15, textTransform: 'uppercase' }}>Order Detail</Text>
                  </View>
                  {/*Loop order Detail */}
                  <FlatList style={{ backgroundColor: 'rgba(255,255,255,0.8)', width: '100%' }}
                    data={this.state.orderdetail}
                    renderItem={({ item }) =>
                      <View style={{ borderBottomColor: '#cdcdcd', borderBottomWidth: 1, width: '100%' }}>
                        <View style={{ width: '50%', height: 25, marginLeft: 10 }}>
                          <Text style={{ fontFamily: 'kanitLight', paddingTop: 5, fontSize: 15 }}>{item.productname}</Text>
                        </View>
                        <View style={{ width: '25%', right: '25%', position: 'absolute', height: 60, marginRight: 10 }}>
                          <Text style={{ fontFamily: 'kanitLight', paddingTop: 5, fontSize: 15, textAlign: 'right' }}>X {item.quantity}</Text>
                        </View>
                        <View style={{ width: '25%', right: 0, position: 'absolute', height: 60, marginRight: 10 }}>
                          <Text style={{ fontFamily: 'kanitLight', paddingTop: 5, fontSize: 15, textAlign: 'right' }}>{item.price}</Text>
                        </View>
                      </View>
                    }
                  />
                </View>
              </View>
            </View>
          </View>



          <Alert
            alertAnimatedIcon
            alertAutoHide
            alertTitle={this.state.alerttitle}
            alertMessage={this.state.alertmessage}
            alertBGColor={this.state.alertcolor}
          />
        </View>
      )
    } else {
      return (
        <View>
          {startloading()}
        </View>
      )
    }
  }//end render



  getOrderDetail(transactionid) {
    const { startloading, stoploading } = this.props;
    startloading();
    //console.log("Customerid" + customerid)
    axios.get(URLSERVICE + `transaction/orderdetail/` + transactionid, { timeout: TIMEOUT })
      .then(res => {
        console.log(res.data),
          this.setState({ orderdetail: res.data })
        stoploading();
      }).catch(err => {
        stoploading();
        this.setState({
          alerttitle: 'Transaction',
          alertmessage: 'Cannot Get Order',
          alertcolor: 'red'
        })
        Alert.showAlert();
      })
  }



}

const mapStateToProps = state => {
  return {
    data: state.app.data,
    user: state.user,
  }
}

const mapDispatchToProps = {
  storeUserData,
  startloading,
  stoploading
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionDetailScreen);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});