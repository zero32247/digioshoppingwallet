import axios from 'axios';
import { STORE_USER_DATA, STORE_MONEY, USER_LOGOUT, STORE_TUTORIAL,STORE_BIOAUTH,STORE_PASSWORD } from '../constants/app';
import {URLSERVICE} from '../config/configvalue';

const storeUser = (username, customerid, firstname, lastname, tel, birthday, walletid, money, secretkey, email, usertype, expoPushToken, logincount) => ({
  type: STORE_USER_DATA,
  username,
  customerid,
  firstname,
  lastname,
  tel,
  birthday,
  walletid,
  money,
  secretkey,
  email,
  usertype,
  expoPushToken,
  logincount,
});

const storePassword = (password) =>({
  type:STORE_PASSWORD,
  password,
})

const storeMoney = (money) => ({
  type: STORE_MONEY,
  money,
})

const storeTutorial = (logincount) => ({
  type: STORE_TUTORIAL,
  logincount,
})

const storeBioAuth = (bioAuth) => ({
  type: STORE_BIOAUTH,
  bioAuth,
})


export const storeUserData = (username, customerid, firstname, lastname, tel, birthday, walletid, money, secretkey, email, usertype, expoPushToken, logincount) => dispatch => {
  // axios.get('https://jsonplaceholder.typicode.com/users')
  //   .then((response) => {
  //console.log("ACTIOn"+username,customerid,firstname,lastname,tel,birthday,walletid,money,secretkey,email,usertype+"Token:   "+expoPushToken+logincount)
  dispatch(storeUser(username, customerid, firstname, lastname, tel, birthday, walletid, money, secretkey, email, usertype, expoPushToken, logincount));
  // })
}

export const storePasswordData = (password) => dispatch =>{
  dispatch(storePassword(password));
}

export const storeMoneyData = (money) => dispatch => {
  dispatch(storeMoney(money));
}

export const storeTutorialData = (username) => dispatch => {
  console.log("STORE TUTORIAL WORKS:    "+username)
  axios.get(URLSERVICE + `user/tooltips/` + username)
    .then(res => {
      console.log("Get Tutorail Complete"+res.data);
      dispatch(storeTutorial(res.data));
    })

}

export const storeBioAuthData = (bioauth) => dispatch =>{
  console.log("STORE BIOAUTH WORKS:    "+bioauth)
  dispatch(storeBioAuth(bioauth));
}

export const userLogout = () => dispatch => {
  dispatch({
    type: USER_LOGOUT
  });
}