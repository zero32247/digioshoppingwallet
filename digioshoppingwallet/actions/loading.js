import axios from 'axios';
import { START_LOADING, STOP_LOADING } from '../constants/app';


export const startloading = () => dispatch => {
    dispatch({
        type: START_LOADING
    });
}
export const stoploading = () => dispatch => {
    dispatch({
        type: STOP_LOADING
    });
}