import { STORE_USER_DATA, STORE_MONEY,STORE_TUTORIAL, USER_LOGOUT,STORE_BIOAUTH,STORE_PASSWORD } from '../constants/app';

const initialState = {
  username: '',
  password:'',
  customerid: '',
  firstname: '',
  lastname: '',
  tel:'',
  birthday:'',
  walletid: '',
  money:'',
  secretkey:'',
  email:'',
  usertype:'',
  expoPushToken : '',
  logincount:'',
  bioAuth:false,
}

export const user = (state = initialState, action) => {
  console.log("ActionType    "+action.type);
  
  switch (action.type) {
    case STORE_USER_DATA:
      console.log("USER REDUCER WORKS")
      return {
        ...state,
        username: action.username,
        customerid: action.customerid,
        firstname: action.firstname,
        lastname: action.lastname,
        tel:action.tel,
        birthday:action.birthday,
        walletid: action.walletid,
        money:action.money,
        secretkey:action.secretkey,
        email:action.email,
        usertype:action.usertype,
        expoPushToken:action.expoPushToken,
        logincount:action.logincount
      }
    case STORE_PASSWORD:
      console.log("PASSWORD REDUCER WORK")
      return{
        ...state,
        password:action.password
      }
    case STORE_MONEY:
      console.log("USER REDUCER WORKS"+action.money)
      return{
        ...state,
        money:action.money
      }
    case STORE_TUTORIAL:
      console.log("TUTORIAL WORK"+action.logincount)
      return{
        ...state,
        logincount:action.logincount
      }
    case STORE_BIOAUTH:
      console.log("BIOAUTH"+action.bioAuth)
      return{
        ...state,
        bioAuth:action.bioAuth
      }
    case USER_LOGOUT:
      console.log("USER LOGOUT REDUCER WORKS")
      return {
        initialState
      }
    default:
      return state;
  }
  
}