import { combineReducers } from 'redux';
import { app } from '../reducers/app';
import { user } from '../reducers/user';
import {loading} from '../reducers/loading';

const rootReducer = combineReducers({
  app,
  user,
  loading
});

export default rootReducer;