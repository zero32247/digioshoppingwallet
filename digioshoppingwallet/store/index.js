import reducers from '../reducers'
import { createLogger } from 'redux-logger'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'
import { persistStore, persistReducer } from "redux-persist";
// import AsyncStorage from '@react-native-community/async-storage';
import {AsyncStorage} from 'react-native';

const middleware = [ thunk ];

if (process.env.NODE_ENV === 'development') {
//   middleware.push(createLogger())
}

// const store = createStore(
//   reducers,
//   applyMiddleware(...middleware)
// );

// configureStore = (initialState = {}) => {
//   const store = createStore(persistReducer({
//     key: 'data',
//     storage: AsyncStorage,
//   }, reducers))
//   const persister = persistStore(store, null);
// }

configureStore = (initialState = {}) => {
  const store = createStore(persistReducer({
    key: 'data',
    storage: AsyncStorage,
  }, reducers), initialState,
  applyMiddleware(...middleware))
  const persister = persistStore(store, null); 
  return { store, persister }
}

export default configureStore;